package com.example.hookUp.fragment.myAds;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.adapter.MyAdsAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.example.hookUp.model.bean.myAdds.MyAddResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.example.hookUp.utils.Constant.tabs.ACTIVE;
import static com.example.hookUp.utils.Constant.tabs.PROCESS;
import static com.example.hookUp.utils.Constant.tabs.SOLD;

public class MyAdsFrag extends Fragment {

    @BindView(R.id.tabs_settlement)
    TabLayout tabLayout;
    @BindView(R.id.pah_my_contest)
    ViewPager viewPager;
    private View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_ads_frag, container, false);
        ButterKnife.bind(this, view);
        intialize();
        return view;
    }

    private void intialize() {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_active)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_process)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_Sold)));
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        rv_adds.setLayoutManager(linearLayoutManager);
        List<MyAddsContestFragment> fragments = new ArrayList<>();
        fragments.add(MyAddsContestFragment.newInstance(ACTIVE));
        fragments.add(MyAddsContestFragment.newInstance(PROCESS));
        fragments.add(MyAddsContestFragment.newInstance(SOLD));
        MyAdsFragmentAdapter adapter = new MyAdsFragmentAdapter(getActivity(), getChildFragmentManager());
        adapter.update(fragments);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }
    @OnClick(R.id.iv_back)
    public void onBackPressed(){
       getActivity().onBackPressed();
    }

}
