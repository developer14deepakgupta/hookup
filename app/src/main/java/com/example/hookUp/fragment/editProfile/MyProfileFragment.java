package com.example.hookUp.fragment.editProfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.utils.CircleTransform;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.ImageLoaderUtils;
import com.github.siyamed.shapeimageview.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyProfileFragment extends Fragment {
    @BindView(R.id.iv_user)
    CircularImageView circleImageView;
    private View view;
    private String type, playerImage;
    private String currentFrag;
    @BindView(R.id.tv_user_fullname)
    TextView tv_user_fullname;
    @BindView(R.id.tv_user_mobile)
    TextView tv_user_mobile;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.tv_gender)
    TextView tv_gender;
    @BindView(R.id.tv_college_name)
    TextView tv_college_name;
    @BindView(R.id.tv_university_name)
    TextView tv_university_name;
    @BindView(R.id.tv_college_stream)
    TextView tv_college_stream;
    @BindView(R.id.tv_university_year)
    TextView tv_university_year;
    @BindView(R.id.tv_bank_name)
    TextView tv_bank_name;
    @BindView(R.id.tv_account_name)
    TextView tv_account_name;
    @BindView(R.id.tv_account_number)
    TextView tv_account_number;
    @BindView(R.id.tv_ifsc_code)
    TextView tv_ifsc_code;
    @BindView(R.id.tv_account_type)
    TextView tv_account_type;
    @BindView(R.id.tv_account_upi)
    TextView tv_account_upi;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_my_profile, container, false);
        ButterKnife.bind(this, view);
        permission();
        intiliaze();
        return view;
    }

    private void intiliaze() {

        tv_user_fullname.setText(HookUpPrefrences.getUserName(getActivity()));
        tv_user_mobile.setText(HookUpPrefrences.getUserMobile(getActivity()));
        tv_email.setText(HookUpPrefrences.getUserEmail(getActivity()));
        tv_gender.setText(HookUpPrefrences.getUserGender(getActivity()));
        tv_college_name.setText(HookUpPrefrences.getUserCollege(getActivity()));
        tv_university_name.setText(HookUpPrefrences.getUserUniversity(getActivity()));
        tv_college_stream.setText(HookUpPrefrences.getUserStream(getActivity()));
        tv_bank_name.setText(HookUpPrefrences.getBankName(getActivity()));
        tv_account_name.setText(HookUpPrefrences.getAccountName(getActivity()));
        tv_ifsc_code.setText(HookUpPrefrences.getIfscCode(getActivity()));
        tv_account_type.setText(HookUpPrefrences.getAccountType(getActivity()));
        tv_account_number.setText(HookUpPrefrences.getUserAccountNumber(getActivity()));

        playerImage = HookUpPrefrences.getUserImage(getActivity());
        if (playerImage != null && !playerImage.isEmpty()) {
            ImageLoaderUtils.load(playerImage, circleImageView, new CircleTransform());
        }
        tv_university_year.setText(HookUpPrefrences.getUserYear(getActivity()));

    }

    private void permission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }


    @OnClick({R.id.iv_back, R.id.iv_edit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if (getActivity() != null)
                    getActivity().onBackPressed();
                break;

            case R.id.iv_edit:
                Fragment editProfileFragment = new EditFragment();
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).loadMainFragment(editProfileFragment, false);
                } else {
                    replaceFragment(editProfileFragment, editProfileFragment.getClass().getSimpleName());
                }
                break;
        }
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }
}
