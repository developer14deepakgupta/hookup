package com.example.hookUp.fragment.myAds;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.adapter.MyAdsAdapter;
import com.example.hookUp.adapter.MyTransitOrderAdapter;
import com.example.hookUp.adapter.PreviewTransitOrderAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.buy.BuyDetailsProductFragment;
import com.example.hookUp.fragment.myOrder.MyOrderFrags;
import com.example.hookUp.fragment.orderApprove.OrderOtpApprovalFrag;
import com.example.hookUp.model.bean.OrderPreview;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.example.hookUp.model.bean.myAdds.MyAddResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;
import timber.log.Timber;

import static com.example.hookUp.fragment.buy.BuyDetailsProductFragment.BUNDLE_KEY_DETAILS;
import static com.example.hookUp.utils.Constant.tabs.ACTIVE;
import static com.example.hookUp.utils.Constant.tabs.PROCESS;
import static com.example.hookUp.utils.Constant.tabs.SOLD;

public class MyAddsContestFragment extends Fragment implements WebServiceResponse,MyAdsAdapter.OnClickListener {
    private View view;
    private MyAdsAdapter adapter;
    private PreviewTransitOrderAdapter previewTransitOrderAdapter;
    private MyDialog myDialog;
    @BindView(R.id.rv_adds)
    RecyclerView rv_adds;
    @BindView(R.id.tv_no_orders)
    TextView tv_no_orders;
    private static final String BUNDLE_KEY_CONTEST_TYPE = "contestType";
    private String type = ACTIVE;
    private String currentFrag;
    int requestType = 1;
    private String auth;
    public static int currentPosition = -1;
    List<MyAddDocs> list_product=new ArrayList<>();

    public static MyAddsContestFragment newInstance(String type) {
        MyAddsContestFragment fragment = new MyAddsContestFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_CONTEST_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_ads_list, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BUNDLE_KEY_CONTEST_TYPE)) {
            type = bundle.getString(BUNDLE_KEY_CONTEST_TYPE);
            intialize();
        }
    }

    private void intialize() {
        myDialog = new MyDialog(getActivity());
         auth= HookUpPrefrences.getAuthorization(getActivity());
        if (GlobalUtils.isNetworkAvailable(getActivity())) {

            if (type.equalsIgnoreCase(ACTIVE)) {
                new WebServiceHandler().getAddActiveList(this, auth, "1");
                requestType = 1;
                myDialog.ShowProgressDialog();
            } else if (type.equalsIgnoreCase(PROCESS)) {
                new WebServiceHandler().getProcessList(this, auth, "1");
                requestType = 2;
                myDialog.ShowProgressDialog();
            } else if (type.equalsIgnoreCase(SOLD)) {
                new WebServiceHandler().getSoldList(this, auth, "1");
                requestType = 3;
                myDialog.ShowProgressDialog();
            }

        }

    }

    private void goToProductDetail(MyAddDocs contest) {
        Fragment buyDetailFrag = new OrderOtpApprovalFrag();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_DETAILS, new Gson().toJson(contest));
        buyDetailFrag.setArguments(bundle);
        Activity activity = getActivity();
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).loadMainFragment(buyDetailFrag, false);
        } else {
            replaceFragment(buyDetailFrag, buyDetailFrag.getClass().getSimpleName());
        }
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        if (requestType == 1 && object instanceof MyAddResponse) {
            MyAddResponse response = (MyAddResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                list_product = response.getBuyProductsList();
                if (list_product != null && list_product.size() > 0) {
                    adapter = new MyAdsAdapter(getActivity(), list_product, "Active");
                    rv_adds.setAdapter(adapter);

                } else if (list_product == null) {
                    tv_no_orders.setText("You haven't listed anything yet");
                    tv_no_orders.setVisibility(View.VISIBLE);
                } else {
                    tv_no_orders.setText("You haven't listed anything yet");
                    tv_no_orders.setVisibility(View.VISIBLE);
                }

            }
        } else if (requestType == 2 && object instanceof MyAddResponse) {
            MyAddResponse response = (MyAddResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                list_product = response.getBuyProductsList();
                if (list_product != null && list_product.size() > 0) {
                    previewTransitOrderAdapter = new PreviewTransitOrderAdapter(getActivity(), list_product, "InProcess");
                    rv_adds.setAdapter(previewTransitOrderAdapter);
                    previewTransitOrderAdapter.setOnClickListener((adapter, position) -> {
                        MyAddDocs contest = adapter.get(position);
                        goToProductDetail(contest);
                    });
                } else if (list_product == null) {
                    tv_no_orders.setText("You don't have any InProcess items");
                    tv_no_orders.setVisibility(View.VISIBLE);

                } else {
                    tv_no_orders.setText("You don't have any InProcess items");
                    tv_no_orders.setVisibility(View.VISIBLE);
                }

            }
        } else if (requestType == 3 && object instanceof MyAddResponse) {
            MyAddResponse response = (MyAddResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                 list_product = response.getBuyProductsList();
                if (list_product != null && list_product.size() > 0) {
                    adapter = new MyAdsAdapter(getActivity(), list_product, "Sold");
                    rv_adds.setAdapter(adapter);
                } else if (list_product == null) {
                    tv_no_orders.setText("You don't have any Sold items");
                    tv_no_orders.setVisibility(View.VISIBLE);
                } else {
                    tv_no_orders.setText("You don't have any Sold items");
                    tv_no_orders.setVisibility(View.VISIBLE);
                }
            }
        }else if (requestType == 4 && object instanceof OrderPreview) {
            OrderPreview response = (OrderPreview) object;
            String message = response.getMessage();
            GlobalUtils.showToast(getActivity(), message);
            Fragment frag = new MyOrderFrags();
            Activity activity = getActivity();
            if (activity instanceof HomeActivity) {
                ((HomeActivity) activity).loadMainFragment(frag, false);
            } else {
                replaceFragment(frag, frag.getClass().getSimpleName());
            }
        }
    }


    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(), getString(R.string.error_api_failure));
        Timber.e(getString(R.string.error_api_failure));

    }

    @Override
    public void onShareClick(MyAdsAdapter adapter, int position) {
        onReferNEarnClick();
    }

    @Override
    public void onDeleteClick(MyAdsAdapter adapter, int position) {
        String product_id=list_product.get(position).getId();
        delteAdd(product_id);
    }
    private void delteAdd(String product_id) {
        new WebServiceHandler().getDeltedSale(this, auth, product_id);
        myDialog.ShowProgressDialog();
        requestType = 4;

    }
    private void onReferNEarnClick() {
        final String appPackageName = getActivity().getApplication().getPackageName();
        String shareBody = "Download " + getString(R.string.app_name) + " From :  " + "http://play.google.com/store/apps/details?id=" + appPackageName;
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.app_name)));
    }
}
