package com.example.hookUp.fragment.orderApprove;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.adapter.ApprovalAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.example.hookUp.model.bean.myAdds.MyAddResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.example.hookUp.fragment.orderApprove.OrderOtpApprovalFrag.BUNDLE_KEY_CONTEST_TYPE;

public class OrderApprovalFragment extends Fragment implements WebServiceResponse {
    private View view;
    @BindView(R.id.rv_order_approval)
    RecyclerView rv_order_approval;
    @BindView(R.id.tv_no_orders)
    TextView tv_no_orders;
    private MyDialog myDialog;
    String currentFrag;
    private ApprovalAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_order_approval, container, false);
        ButterKnife.bind(this, view);
        intialize();
        return view;
    }

    private void intialize() {
        myDialog = new MyDialog(getActivity());
        adapter = new ApprovalAdapter(getActivity());
        rv_order_approval.setAdapter(adapter);
        adapter.setOnClickListener((adapter, position) -> {
            MyAddDocs contest = adapter.get(position);
            goToApprovalDetails(contest);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getorderList();
    }

    private void getorderList() {
        String auth = HookUpPrefrences.getAuthorization(getActivity());
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            new WebServiceHandler().getProcessList(this, auth, "1");
            myDialog.ShowProgressDialog();
        }
    }

    private void goToApprovalDetails(MyAddDocs contest) {
        Fragment otpfrag = new OrderOtpApprovalFrag();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_CONTEST_TYPE, new Gson().toJson(contest));
        otpfrag.setArguments(bundle);
        Activity activity = getActivity();
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).loadMainFragment(otpfrag, false);
        } else {
            replaceFragment(otpfrag, otpfrag.getClass().getSimpleName());
        }

    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        MyAddResponse response = (MyAddResponse) object;
        int ack = response.getAck();
        if (ack == 1) {
            List<MyAddDocs> list_product = response.getBuyProductsList();
            if (list_product != null && list_product.size() > 0) {
                adapter.update(list_product);
            } else {
                tv_no_orders.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(),"connection Failed");
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }
    @OnClick(R.id.iv_back)
    public void onBackPressed() {
        getActivity().onBackPressed();
    }
}
