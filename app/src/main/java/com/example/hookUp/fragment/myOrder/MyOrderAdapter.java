package com.example.hookUp.fragment.myOrder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.fragment.buy.BuyDetailAdapter;
import com.example.hookUp.model.bean.MyOrderList;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyViewHolder> {
    private List<MyOrderList> orders;
    private Context context;
    private OnClickListener listener;

    public MyOrderAdapter(Context context) {
        this.context = context;
        this.orders = new ArrayList<>();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list, parent, false);
        return new MyOrderAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MyOrderList list = orders.get(position);
        if (orders.get(position) != null) {
            if (list.getSale() != null) {
                if (list.getSale().getTransactions() != null) {
                    for (int i = 0; i < list.getSale().getTransactions().size(); i++) {
                        if (list.getSale().getTransactions().get(i).getOrderId() != null)
                            holder.tv_order_number.setText(list.getSale().getTransactions().get(i).getOrderId());
                        if (list.getSale().getTransactions().get(i).getTransaction_date() != null)
                            holder.tv_ordered_date.setText(list.getSale().getTransactions().get(i).getTransaction_date());
                        if (list.getSale().getTransactions().get(i).getPayment_mode() != null)
                            holder.tv_order_payment_method.setText(list.getSale().getTransactions().get(i).getPayment_mode());
                    }
                }
                if (list.getSale().getPrice() != null)
                    holder.tv_amount_paid.setText(list.getSale().getPrice());
                if (list.getSale().getImage_a() != null)
                    ImageLoaderUtils.load(IMAGE_PATH + list.getSale().getImage_a(), holder.iv_order_prod_image);

            }
        }

    }

    public void update(List<MyOrderList> list) {
        this.orders.clear();
        this.orders.addAll(list);
        notifyDataSetChanged();
    }

    public MyOrderList get(int position) {
        return orders.get(position);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onClick(MyOrderAdapter adapter, int position);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_order_no)
        TextView tv_order_number;
        @BindView(R.id.tv_ordered_date)
        TextView tv_ordered_date;
        @BindView(R.id.tv_amount_paid)
        TextView tv_amount_paid;
        @BindView(R.id.tv_order_payment_method)
        TextView tv_order_payment_method;
        @BindView(R.id.iv_order_prod_image)
        ImageView iv_order_prod_image;
        private OnClickListener listener;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
