package com.example.hookUp.fragment.myOrder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.adapter.MyTransitOrderAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.MyOrderList;
import com.example.hookUp.model.bean.MyOrderResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static com.example.hookUp.fragment.buy.BuyDetailsProductFragment.BUNDLE_KEY_DETAILS;
import static com.example.hookUp.utils.Constant.tabs.ACTIVE;
import static com.example.hookUp.utils.Constant.tabs.PROCESS;
import static com.example.hookUp.utils.Constant.tabs.SOLD;

public class MyOrderFragment extends Fragment implements WebServiceResponse {
    @BindView(R.id.rvOrder)
    RecyclerView orderRv;
    private View view;
    private MyDialog myDialog;
    MyOrderAdapter myOrderAdapter;
    @BindView(R.id.tv_no_orders)
    TextView lblOrder;
    @BindView(R.id.tv_nothing_here)
    TextView tv_nothing_here;
    private String type = PROCESS;
    private String currentFrag;
    private static final String BUNDLE_KEY_CONTEST_TYPE = "contestType";
    int requestType = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_order_frag, container, false);
        ButterKnife.bind(this, view);
        intialize();
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BUNDLE_KEY_CONTEST_TYPE)) {
            type = bundle.getString(BUNDLE_KEY_CONTEST_TYPE);
            intialize();
        }

        return view;
    }

    public static MyOrderFragment newInstance(String type) {
        MyOrderFragment fragment = new MyOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_CONTEST_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void intialize() {
        myDialog = new MyDialog(getActivity());
        orderRv.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        orderRv.setLayoutManager(manager);
        myOrderAdapter = new MyOrderAdapter(getActivity());
        orderRv.setAdapter(myOrderAdapter);


    }

    private void goToProductDetail(MyOrderList contest) {
        Fragment buyDetailFrag = new PreviewAvailableFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_DETAILS, new Gson().toJson(contest));
        buyDetailFrag.setArguments(bundle);
        Activity activity = getActivity();
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).loadMainFragment(buyDetailFrag, false);
        } else {
            replaceFragment(buyDetailFrag, buyDetailFrag.getClass().getSimpleName());
        }
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String auth = HookUpPrefrences.getAuthorization(getActivity());
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            if (type.equalsIgnoreCase(PROCESS)) {
                new WebServiceHandler().getOrderallList(this, auth);
                requestType = 1;
                myDialog.ShowProgressDialog();
            } else if (type.equalsIgnoreCase(ACTIVE)) {
                new WebServiceHandler().getOrderActiveList(this, auth, "1");
                requestType = 2;
                myDialog.ShowProgressDialog();
            } else if (type.equalsIgnoreCase(SOLD)) {
                new WebServiceHandler().getOrderRejectedList(this, auth, "1");
                requestType = 3;
                myDialog.ShowProgressDialog();
            }
        }
    }


    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();

        if (requestType == 1 && object instanceof MyOrderResponse) {

            MyOrderResponse response = (MyOrderResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                MyTransitOrderAdapter myTransitOrderAdapter = new MyTransitOrderAdapter(getActivity());
                List<MyOrderList> order_all_list = new ArrayList<>();
                order_all_list = response.getBuyProductsList();
                if (order_all_list != null && order_all_list.size() > 0) {
                    order_all_list = response.getBuyProductsList();
                    myTransitOrderAdapter.update(order_all_list);
                    orderRv.setAdapter(myTransitOrderAdapter);
                    lblOrder.setVisibility(View.GONE);
                } else if (order_all_list == null) {
                    lblOrder.setText("You don't have any Ordered item");
                    lblOrder.setVisibility(View.VISIBLE);
                    tv_nothing_here.setVisibility(View.VISIBLE);
                } else {
                    lblOrder.setText("You don't have any Ordered item");
                    lblOrder.setVisibility(View.VISIBLE);
                    tv_nothing_here.setVisibility(View.VISIBLE);
                }
                myTransitOrderAdapter.setOnClickListener((adapter, position) -> {
                    MyOrderList contest = adapter.get(position);
                    goToProductDetail(contest);
                });
            }

        } else if (requestType == 2 && object instanceof MyOrderResponse) {
            MyOrderResponse response = (MyOrderResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                List<MyOrderList> order_active__list = new ArrayList<>();
                order_active__list = response.getBuyProductsList();
                if (order_active__list != null && order_active__list.size() > 0) {
                    order_active__list = response.getBuyProductsList();
                    myOrderAdapter.update(order_active__list);
                    lblOrder.setVisibility(View.GONE);
                } else if (order_active__list == null) {
                    lblOrder.setText("You don't have any Approved item");
                    lblOrder.setVisibility(View.VISIBLE);
                    tv_nothing_here.setVisibility(View.VISIBLE);
                } else {
                    lblOrder.setText("You don't have any Approved item");
                    lblOrder.setVisibility(View.VISIBLE);
                    tv_nothing_here.setVisibility(View.VISIBLE);
                }
            }
        } else if (requestType == 3 && object instanceof MyOrderResponse) {
            MyOrderResponse response = (MyOrderResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                List<MyOrderList> order_reject__list = new ArrayList<>();
                order_reject__list = response.getBuyProductsList();
                if (order_reject__list != null && order_reject__list.size() > 0) {
                    order_reject__list = response.getBuyProductsList();
                    myOrderAdapter.update(order_reject__list);
                } else if (order_reject__list == null) {
                    lblOrder.setText("You don't have any Rejected item");
                    lblOrder.setVisibility(View.VISIBLE);
                    tv_nothing_here.setVisibility(View.VISIBLE);
                } else {
                    lblOrder.setText("You don't have any Rejected item");
                    lblOrder.setVisibility(View.VISIBLE);
                    tv_nothing_here.setVisibility(View.VISIBLE);
                }
            }
        }


    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(), "connection Failed");
    }
}
