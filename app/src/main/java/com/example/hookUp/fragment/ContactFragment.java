package com.example.hookUp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.activity.login.LoginActivity;
import com.example.hookUp.activity.registration.RegistrationActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.home.NewHomeFragment;
import com.example.hookUp.model.bean.ForgotResponse;
import com.example.hookUp.model.bean.finduniversityResponse.yearResponse.YearDetail;
import com.example.hookUp.model.bean.query.QueryData;
import com.example.hookUp.model.bean.query.QueryResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class ContactFragment extends Fragment implements WebServiceResponse {
    private MyDialog myDialog;
    private View view;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.et_message)
    EditText et_message;
    String message;
    String currentFrag = "", query_id = "";
    private int RequestType = 0;
    @BindView(R.id.spnr_purpose)
    Spinner spnr_purpose;
    private List<QueryData> query_list = new ArrayList<>();
    private List<String> query_type = new ArrayList<>();


    public ContactFragment() {
// Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_frag, container, false);
        ButterKnife.bind(this, view);
        myDialog = new MyDialog(getActivity());
        intialize();
        return view;
    }

    private void intialize() {
        query_type.add(0, "Select Purpose");
        spnr_purpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String section_selected = parent.getSelectedItem() + "";
                    query_id = query_list.get(position - 1).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @OnClick({R.id.iv_back, R.id.btn_submit})
    public void onBackPressed(View view) {
        if (view.getId() == R.id.iv_back) {
            getActivity().onBackPressed();
        } else if (view.getId() == R.id.btn_submit) {
            if (GlobalUtils.isNetworkAvailable(getActivity())) {
                if (validation()) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("query_id", query_id);
                    map.put("msg", message);
                    new WebServiceHandler().callcontactUsAPI(this, HookUpPrefrences.getAuthorization(getActivity()), map);
                    RequestType = 2;
                    myDialog.ShowProgressDialog();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getQuery();
    }

    private void getQuery() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            new WebServiceHandler().callgetQueryAPI(this);
            RequestType = 1;
        }
    }

    private boolean validation() {
        message = et_message.getText().toString();
        if (message.isEmpty()) {
            GlobalUtils.showToast(getActivity(), getString(R.string.message));
            return false;
        }
        return true;
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();

        if (RequestType == 1 && object instanceof QueryResponse) {
            QueryResponse response = (QueryResponse) object;
            int ack = response.getAck();
            String message = response.getMessage();
            if (ack == 1) {
                query_list = new ArrayList<>();
                query_list = response.getData();
                query_type.clear();
                query_type.add(0, "Select Purpose");
                for (int i = 0; i < query_list.size(); i++) {
                    String queryName = query_list.get(i).getMessage();
                    query_type.add(queryName);
                }
                ArrayAdapter<String> adapter_query = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, query_type);
                adapter_query.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_purpose.setAdapter(adapter_query);
            }

        } else if (RequestType == 2 && object instanceof ForgotResponse) {
            ForgotResponse forgotResponse = (ForgotResponse) object;
            int ack = forgotResponse.getAck();
            String message = forgotResponse.getMsg();
            if (ack == 1) {
                GlobalUtils.showToastSuccess(getActivity(), message);
                Fragment homeFragment = new NewHomeFragment();
                currentFrag = "Contact";
                Activity activity = getActivity();
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).loadFragment(homeFragment, true);
                } else {
                    replaceFragment(homeFragment, currentFrag);
                }
            }

        }


    }

    @Override
    public void onErrorHandling(Response response) {

    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(),"connection Failed");
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }
}
