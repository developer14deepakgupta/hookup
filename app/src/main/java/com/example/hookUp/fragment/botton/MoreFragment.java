package com.example.hookUp.fragment.botton;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.fragment.ContactFragment;
import com.example.hookUp.fragment.HelpFragment;
import com.example.hookUp.fragment.myOrder.MyOrderFragment;
import com.example.hookUp.fragment.myOrder.MyOrderFrags;
import com.example.hookUp.fragment.orderApprove.OrderApprovalFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MoreFragment extends Fragment {
    private String currentFrag = "";
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_more, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.iv_back)
    public void onclickBack(View view) {
        if (getActivity() != null)
            getActivity().onBackPressed();
    }

    @OnClick({R.id.pma_my_order, R.id.pma_rate_us, R.id.pma_refer, R.id.pma_help, R.id.pma_contact})
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        Fragment fragment;
        if (view.getId() == R.id.pma_my_order) {
            fragment = new MyOrderFrags();
            if (getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).loadMainFragment(fragment, false);
            } else {
                replaceFragment(fragment, fragment.getClass().getSimpleName());
            }
        } else if (view.getId() == R.id.pma_rate_us) {
            fragment = new MyOrderFragment();
            if (getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).loadMainFragment(fragment, false);
            } else {
                replaceFragment(fragment, fragment.getClass().getSimpleName());
            }
        } else if (view.getId() == R.id.pma_refer) {
            onReferNEarnClick();
        } else if (view.getId() == R.id.pma_help) {
            fragment = new HelpFragment();
            bundle.putString("sessionUrl", "http://13.126.248.29:1221/About_Us.pdf");
            fragment.setArguments(bundle);
            if (getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).loadMainFragment(fragment, false);
            } else {
                replaceFragment(fragment, fragment.getClass().getSimpleName());
            }
        } else if (view.getId() == R.id.pma_contact) {
            fragment = new ContactFragment();
            bundle.putString("sessionUrl", "http://13.126.248.29:1221/About_Us.pdf");
            fragment.setArguments(bundle);
            if (getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).loadMainFragment(fragment, false);
            } else {
                replaceFragment(fragment, fragment.getClass().getSimpleName());
            }
        }
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String name) {
        currentFrag = name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, name);
        fragmentTransaction.addToBackStack(name);
        fragmentTransaction.commit();
    }

    private void onReferNEarnClick() {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.msg_download_dealer));
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Refer to HookUp");
            sendIntent.setType("text/plain");
            Intent shareIntent = Intent.createChooser(sendIntent, "HookUp");
            startActivity(shareIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
