package com.example.hookUp.fragment.editProfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.BankResponse;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.utils.BaseClass;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.ImageLoaderUtils;
import com.example.hookUp.utils.MyDialog;
import com.example.hookUp.utils.RealPathUtils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.example.hookUp.utils.Constant.FEMALE;
import static com.example.hookUp.utils.Constant.MALE;
import static com.example.hookUp.utils.Constant.TYPE_CURRENT;
import static com.example.hookUp.utils.Constant.TYPE_SAVING;

public class EditFragment extends Fragment implements WebServiceResponse {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.iv_user)
    CircularImageView circleImageView;
    @BindView(R.id.tv_gender)
    TextView tv_gender;
    @BindView(R.id.et_bank_name)
    EditText et_bank_name;
    @BindView(R.id.et_bank_account_name)
    EditText et_bank_account_name;
    @BindView(R.id.et_bank_account_number)
    EditText et_bank_account_number;
    @BindView(R.id.et_bank_ifsc)
    EditText et_bank_ifsc;
    @BindView(R.id.et_bank_upi)
    EditText et_bank_upi;
    @BindView(R.id.et_firstname)
    EditText et_firstname;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.rb_saving)
    RadioButton rb_saving;
    @BindView(R.id.rb_current)
    RadioButton rb_current;
    @BindView(R.id.radio_group)
    RadioGroup radio_group;
    private String[] valuesGender = new String[]{MALE, FEMALE};
    private View view;
    private String type;
    private String token;
    // Uplooad Camera
    private Dialog myDialog;
    private static final int PICKFILE_RESULT_CODE = 1;
    private static final int PICKIMAGE_RESULT_CODE = 2;
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    private Uri imageUri;
    private String mCurrentPhotoPath;
    private String file_path = "";
    private String currentFrag;
    private File fileToUpload;
    private MyDialog progress;
    //    private List<UniversityDetail> universitylist = new ArrayList<>();
//    private List<CollegeDetail> collegelist = new ArrayList<>();
//    private List<CourseDetail> courselist = new ArrayList<>();
//    private List<StreamDetail> streamlist = new ArrayList<>();
//    private List<YearDetail> yearlist = new ArrayList<>();
//
//    ArrayList<String> university_type = new ArrayList<>();
//    ArrayList<String> college_type = new ArrayList<>();
//    ArrayList<String> course_type = new ArrayList<>();
//    ArrayList<String> stream_type = new ArrayList<>();
//    ArrayList<String> year_type = new ArrayList<>();
//    int universityId = 0;
    int requestType = 0;
    static Boolean isTouched = false;
    private String unversityId = "0", collegeId = "0", courseId = "1", streamId = "1", yearIds = "", yearId = "1", dAddress = "Y";
    private String account_no, bank_name, account_name, ifscCode, upi_id;

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_profile, container, false);
        ButterKnife.bind(this, view);
        intialize();
//        getuniversity();
        return view;
    }

//    private void getuniversity() {
//        if (GlobalUtils.isNetworkAvailable(getActivity())) {
//            new WebServiceHandler().getUniversityList(this, "0");
//            requestType = 1;
//
//        }
//    }

    private void intialize() {
        progress = new MyDialog(getActivity());
        tv_name.setText(getString(R.string.my_edit_profile));
        token = HookUpPrefrences.getAuthorization(getActivity());
        et_firstname.setText(HookUpPrefrences.getUserName(getActivity()));
        et_email.setText(HookUpPrefrences.getUserEmail(getActivity()));
        et_phone.setText(HookUpPrefrences.getUserMobile(getActivity()));
        tv_gender.setOnClickListener(v -> {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle("Select Gender:");
            alertDialogBuilder.setItems(valuesGender, (dialog, which) -> {
                String selectedText = Arrays.asList(valuesGender).get(which);
                tv_gender.setText(selectedText);
            });
            android.app.AlertDialog dialog = alertDialogBuilder.create();
            dialog.show();
        });

//        //  tv_year.setOnClickListener(v -> createDatePickerDialog(this, tv_year, ""));
//        university_type.add(0, "Select University");
//        college_type.add(0, "Select College");
//        course_type.add(0, "Select Course");
//        stream_type.add(0, "Select Stream");
//        year_type.add(0, "Select Year");


//        spnr_university.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    String section_selected = parent.getSelectedItem() + "";
//                    String name = universitylist.get(position - 1).getUniversityName();
//                    unversityId = universitylist.get(position - 1).getId();
//                    if (GlobalUtils.isNetworkAvailable(getActivity())) {
//                        new WebServiceHandler().getCollegeList(EditFragment.this, "0");
//                        requestType = 2;
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spnr_college.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    String section_selected = parent.getSelectedItem() + "";
//                    String name = collegelist.get(position - 1).getCollege_name();
//                    collegeId = collegelist.get(position - 1).getId();
//                    if (GlobalUtils.isNetworkAvailable(getActivity())) {
//                        new WebServiceHandler().getCourseList(EditFragment.this, "1");
//                        requestType = 3;
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        // spnr course
//        spnr_course.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    String section_selected = parent.getSelectedItem() + "";
//                    String name = courselist.get(position - 1).getCourse_name();
//                    courseId = courselist.get(position - 1).getId();
//                    yearIds = courselist.get(position - 1).getYear_ids();
//                    if (GlobalUtils.isNetworkAvailable(getActivity())) {
//                        new WebServiceHandler().getStreamList(EditFragment.this, "1");
//                        requestType = 4;
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        //spnr stream
//        spnr_stream.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    String section_selected = parent.getSelectedItem() + "";
//                    String name = streamlist.get(position - 1).getStream_name();
//                    yearId = streamlist.get(position - 1).getId();
//                    if (GlobalUtils.isNetworkAvailable(getActivity())) {
//                        new WebServiceHandler().getYearList(EditFragment.this, yearIds);
//                        requestType = 5;
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//
//        });
//
//        //spnr year
//        spnr_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    String section_selected = parent.getSelectedItem() + "";
//                    String name = yearlist.get(position - 1).getYear_name();
//                    yearId = yearlist.get(position - 1).getId();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

//        switch_item.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                isTouched = true;
//                return false;
//            }
//        });
//        switch_item.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isTouched) {
//                    isTouched = false;
//                    if (isChecked) {
//                        dAddress = "Y";
//                        Log.e("defaultChecked-->", dAddress);
//                    } else {
//                        dAddress = "N";
//                        Log.e("defaultUnChecked-->", dAddress);
//                    }
//                }
//            }
//        });

//        iv_plus.setOnClickListener((View view) -> {
//            lin_add_details.setVisibility(View.VISIBLE);
//            iv_minus.setVisibility(View.VISIBLE);
//        });

    }

    @OnClick({R.id.tv_change_avatar, R.id.iv_back, R.id.btn_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_change_avatar:
                type = "SelfImage";
                // showFileChooser1();
                uploadFilesCamera("Upload Profile");
                break;
            case R.id.btn_submit:
                if (bankvalidation()) {
                    hitBankDetails();

                }
                break;
            case R.id.iv_back:
                if (getActivity() != null)
                    getActivity().onBackPressed();
                break;
        }


    }

    private void uploadFilesCamera(String title) {
        myDialog = new Dialog(getActivity());
        myDialog.setContentView(R.layout.dialog_upload_file);
        TextView textPopUp = myDialog.findViewById(R.id.textPopUp);
        textPopUp.setText(title);
        ImageView imageView3 = myDialog.findViewById(R.id.imageView3);
        imageView3.setOnClickListener(this::onClick);
        LinearLayout camera = myDialog.findViewById(R.id.camera);
        LinearLayout attach = myDialog.findViewById(R.id.attach);
        attach.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, PICKFILE_RESULT_CODE);
        });
        camera.setOnClickListener(v -> {
            try {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_STORAGE_PERMISSION);
                } else {
                    launchCamera(PICKIMAGE_RESULT_CODE);
                }
            } catch (Exception e) {
                Timber.e(e);
            }
        });
        imageView3.setOnClickListener(v -> {
            if (myDialog.isShowing()) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }


    private void launchCamera(int requestImage) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "MyPicture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "Photo taken on " + System.currentTimeMillis());
        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, requestImage);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp;
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File file = File.createTempFile(
                imageFileName,   //prefix
                ".jpg",          //suffix
                storageDir       //directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = file.getAbsolutePath();
        return file;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:          //from file manager
                if (resultCode == RESULT_OK) {
                    try {
                        String filePath = BaseClass.getMimeType(getActivity(), data.getData());
                        if (filePath.equalsIgnoreCase("png") || filePath.equalsIgnoreCase("jpeg") ||
                                filePath.contains("jpg")) {
                            if (myDialog.isShowing()) {
                                myDialog.dismiss();
                            }
                            imageUri = data.getData();
                            if (imageUri != null)
                                try {
                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                                    saveImage2(bitmap, imageUri);
                                } catch (IOException e) {
                                    Timber.e(e);
                                }
                        } else {
                            GlobalUtils.showToast(getActivity(), "Please Upload Image");
                        }
                    } catch (Exception e) {
                        Timber.e(e);
                    }
                }
                break;
            case PICKIMAGE_RESULT_CODE:
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                if (resultCode == RESULT_OK) {
                    Bitmap bitmap;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                        saveImage2(bitmap, imageUri);
                    } catch (IOException e) {
                        Timber.e(e);
                    }
                }
                break;

        }

    }

    private void saveImage2(Bitmap myBitmap, Uri image) {
        try {
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                fileToUpload = new File(image.getPath());
                RealPathUtils filePathHelper = new RealPathUtils();
                String filePath;
                if (Build.VERSION.SDK_INT >= 23) {
                    if (filePathHelper.getPathnew(image, getActivity()) != null) {
                        filePath = filePathHelper.getPathnew(image, getActivity()).toLowerCase();
                    } else {
                        filePath = filePathHelper.getFilePathFromURI(image, getActivity()).toLowerCase();
                    }
                } else {
                    filePath = filePathHelper.getPath(image, getActivity()).toLowerCase();
                }
                file_path = getFileName(image);
                mCurrentPhotoPath = filePath;
                fileToUpload = new File(mCurrentPhotoPath);
                ImageLoaderUtils.load(image.toString(), circleImageView);
                HookUpPrefrences.setUserImage(getActivity(), image.toString());
                UploadAvatar();
                //  myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
            } catch (Exception e) {
                Timber.e(e);
            }


        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void UploadAvatar() {

    }


    private String getFileName(Uri uri) {
        String realPath = uri.getPath();
        Timber.d("realPath %s", realPath);
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
                Timber.d("ResulTwo %s", result);
            }
        }
        return result;
    }

    private boolean bankvalidation() {
        account_name = et_bank_account_name.getText().toString();
        account_no = et_bank_account_number.getText().toString();
        bank_name = et_bank_name.getText().toString();
        ifscCode = et_bank_ifsc.getText().toString();
        upi_id = et_bank_upi.getText().toString();
        type = TYPE_SAVING;

        if (GlobalUtils.isNullOrEmpty(account_name)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.empty_account_name));
            return false;
        } else if (GlobalUtils.isNullOrEmpty(account_no)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.empty_account_number));
            return false;
        } else if (GlobalUtils.isNullOrEmpty(bank_name)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.empty_bank_name));
            return false;
        } else if (GlobalUtils.isNullOrEmpty(ifscCode)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.empty_ifsc_code));
            return false;
        } else if (!GlobalUtils.isIfscCodeValid(ifscCode)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.valid_ifsc_code));
            return false;
        } else if (radio_group.getCheckedRadioButtonId() == -1) {
            GlobalUtils.showToast(getActivity(), getString(R.string.altest_one_entity));
            return false;
        } else if (rb_current.isChecked()) {
            type = TYPE_CURRENT;
            return false;
        }

        return true;
    }
//    private void hitEductionDetails() {
//        if (GlobalUtils.isNetworkAvailable(getActivity())) {
//            String token = HookUpPrefrences.getAuthorization(getActivity());
//            HashMap<String, String> map = new HashMap<>();
//            map.put("university_id", unversityId);
//            map.put("college_id", collegeId);
//            map.put("course_id", courseId);
//            map.put("stream_id ", streamId);
//            map.put("year_id  ", yearId);
//            new WebServiceHandler().callAddEducationAPI(this, token, map);
//            requestType = 6;
//        }
//    }

    private void hitBankDetails() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            map.put("account_name", account_name);
            map.put("account_no", account_no);
            map.put("bank_name", bank_name);
            map.put("account_type", type);
            map.put("ifsc_code", ifscCode);
            map.put("upi_id", upi_id);
            new WebServiceHandler().callAddBankAPI(this, token, map);
            progress.ShowProgressDialog();
            requestType = 7;
        }
    }

    private boolean validation() {
        if (unversityId.isEmpty()) {
            GlobalUtils.showToastSuccess(getActivity(), getString(R.string.university_valid));
            return false;
        } else if (collegeId.isEmpty()) {
            GlobalUtils.showToastSuccess(getActivity(), getString(R.string.college_valid));
            return false;
        } else if (courseId.isEmpty()) {
            GlobalUtils.showToastSuccess(getActivity(), getString(R.string.course_valid));
            return false;
        } else if (streamId.isEmpty()) {
            GlobalUtils.showToastSuccess(getActivity(), getString(R.string.stream_valid));
            return false;
        } else if (yearId.isEmpty()) {
            GlobalUtils.showToastSuccess(getActivity(), getString(R.string.year_valid));
            return false;
        }
        return true;
    }

    @Override
    public void OnSuccess(Object object) {
        progress.CancelProgressDialog();
//        if (requestType == 1 && object instanceof UniversityResponse) {
//            UniversityResponse universityResponse = (UniversityResponse) object;
//            int ack = universityResponse.getAck();
//            if (ack == 1) {
//                universitylist = new ArrayList<>();
//                universitylist = universityResponse.getData();
//                university_type.clear();
//                university_type.add(0, "Select University");
//                for (int i = 0; i < universitylist.size(); i++) {
//                    String unversityName = universitylist.get(i).getUniversityName();
//                    university_type.add(unversityName);
//                }
//                ArrayAdapter<String> adapterUnivsersity = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, university_type);
//                adapterUnivsersity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spnr_university.setAdapter(adapterUnivsersity);
//
//            }
//
//        } else if (requestType == 2 && object instanceof CollegeResponse) {
//            CollegeResponse collegeResponse = (CollegeResponse) object;
//            int ack = collegeResponse.getAck();
//            if (ack == 1) {
//                collegelist = new ArrayList<>();
//                collegelist = collegeResponse.getData();
//                college_type.clear();
//                college_type.add(0, "Select College");
//                for (int i = 0; i < collegelist.size(); i++) {
//                    String unversityName = collegelist.get(i).getCollege_name();
//                    college_type.add(unversityName);
//                }
//                ArrayAdapter<String> adapterCollege = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, college_type);
//                adapterCollege.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spnr_college.setAdapter(adapterCollege);
//            }
//        } else if (requestType == 3 && object instanceof CourseResponse) {
//            CourseResponse courseResponse = (CourseResponse) object;
//            int ack = courseResponse.getAck();
//            if (ack == 1) {
//                courselist = new ArrayList<>();
//                courselist = courseResponse.getData();
//                course_type.clear();
//                course_type.add(0, "Select Course");
//                for (int i = 0; i < courselist.size(); i++) {
//                    String unversityName = courselist.get(i).getCourse_name();
//                    course_type.add(unversityName);
//                }
//                ArrayAdapter<String> adapterCourse = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, course_type);
//                adapterCourse.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spnr_course.setAdapter(adapterCourse);
//            }
//        } else if (requestType == 4 && object instanceof StreamReponse) {
//            StreamReponse streamReponse = (StreamReponse) object;
//            int ack = streamReponse.getAck();
//            if (ack == 1) {
//                streamlist = new ArrayList<>();
//                streamlist = streamReponse.getData();
//                stream_type.clear();
//                stream_type.add(0, "Select Stream");
//                for (int i = 0; i < streamlist.size(); i++) {
//                    String unversityName = streamlist.get(i).getStream_name();
//                    stream_type.add(unversityName);
//                }
//                ArrayAdapter<String> adapterStream = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, stream_type);
//                adapterStream.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spnr_stream.setAdapter(adapterStream);
//            }
//        } else if (requestType == 5 && object instanceof YearResponse) {
//            YearResponse yearResponse = (YearResponse) object;
//            int ack = yearResponse.getAck();
//            if (ack == 1) {
//                yearlist = new ArrayList<>();
//                yearlist = yearResponse.getData();
//                year_type.clear();
//                year_type.add(0, "Select Year");
//                for (int i = 0; i < yearlist.size(); i++) {
//                    String unversityName = yearlist.get(i).getYear_name();
//                    year_type.add(unversityName);
//                }
//                ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, android.R.id.text1, year_type);
//                adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spnr_year.setAdapter(adapterYear);
//            }
//        } else if (requestType == 6 && object instanceof RegistrationResponse) {
//            RegistrationResponse response = (RegistrationResponse) object;
//            int ack = response.getAck();
//            String message = response.getMsg();
//            if (ack == 1) {
//                GlobalUtils.showToastSuccess(getActivity(), message);
//                lin_add_details.setVisibility(GONE);
//                iv_plus.setVisibility(View.VISIBLE);
//                iv_minus.setVisibility(GONE);
//
//            }
        //}
        if (requestType == 7 && object instanceof BankResponse) {
            BankResponse response = (BankResponse) object;
            int ack = response.getAck();
            String message = response.getMsg();
            if (ack == 1) {
                HookUpPrefrences.setBankName(getActivity(), response.getBankData().getBank_name());
                HookUpPrefrences.setUserAccountNumber(getActivity(), response.getBankData().getAccount_no());
                HookUpPrefrences.setAccountName(getActivity(), response.getBankData().getAccount_name());
                HookUpPrefrences.setAccountType(getActivity(), response.getBankData().getAccount_type());
                HookUpPrefrences.setUpiId(getActivity(), response.getBankData().getUpi_id());
                HookUpPrefrences.setIfscCode(getActivity(), response.getBankData().getIfsc_code());
                GlobalUtils.showToastSuccess(getActivity(), message);
                Fragment recentFrag = new MyProfileFragment();
                Activity activity = getActivity();
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).loadFragment(recentFrag, false);
                } else {
                    replaceFragment(recentFrag, recentFrag.getClass().getSimpleName());
                }
            }
        }


    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void onErrorHandling(Response response) {
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(getActivity(), errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        GlobalUtils.showToast(getActivity(), getString(R.string.error_api_failure));
        Timber.e(getString(R.string.error_api_failure));
    }
}
