package com.example.hookUp.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.home.NewHomeFragment;
import com.example.hookUp.model.bean.SellResponse;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategoryResposne;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategotyDetail;
import com.example.hookUp.utils.BaseClass;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.example.hookUp.utils.RealPathUtils;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class SellFragment extends Fragment implements WebServiceResponse {
    @BindView(R.id.iv_plus)
    ImageView iv_plus;
    @BindView(R.id.iv_sell_images_one)
    ImageView iv_sell_images_one;
    @BindView(R.id.iv_sell_images_two)
    ImageView iv_sell_images_two;
    @BindView(R.id.iv_sell_images_three)
    ImageView iv_sell_images_three;
    @BindView(R.id.et_title)
    EditText et_title;
    @BindView(R.id.et_price)
    EditText et_price;
    @BindView(R.id.et_product_description)
    EditText et_product_description;
    @BindView(R.id.tv_category)
    TextView tv_category;
    private ArrayList<String> myStateList = new ArrayList<>();
    private Uri imageUri;
    private View view;
    int setPhoto = 0;
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private Dialog myDialog;
    private String s_file_name;
    private File imageFile;
    String categoryId = "", productTitle = "", productPrice = "", productDescription;
    private int requestType = 1;
    int catId;
    private String currentFrag;
    ArrayList<String> filePaths = new ArrayList<>();
    boolean set_image_one = true, set_image_two = true, set_image_three = true;
    private MyDialog progress;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_sell_description, container, false);
        ButterKnife.bind(this, view);
        permission();
        intialize();
        return view;
    }

    private void intialize() {
        progress = new MyDialog(getActivity());
        tv_category.setOnClickListener(v -> {
            if (myStateList != null && myStateList.size() > 0) {
                myStateList.clear();
            }
            listcategory();
        });
    }

    private void permission() {
        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    private void listcategory() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            new WebServiceHandler().getCategoryList(this, "0");
            progress.ShowProgressDialog();
            requestType = 1;

        }
    }

    private void uploadFilesCamera() {
        myDialog = new Dialog(getActivity());
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        myDialog.setContentView(R.layout.dialog_upload_file);
        TextView textPopUp = myDialog.findViewById(R.id.textPopUp);
        ImageView imageView3 = myDialog.findViewById(R.id.imageView3);
        imageView3.setOnClickListener(this::onClick);
        LinearLayout camera = myDialog.findViewById(R.id.camera);
        LinearLayout attach = myDialog.findViewById(R.id.attach);
        attach.setOnClickListener(v -> {
            myDialog.dismiss();
            openGallery();
        });
        camera.setOnClickListener(v -> {
            try {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_STORAGE_PERMISSION);
                } else {
                    myDialog.dismiss();
                    takePicture(REQUEST_CODE_TAKE_PICTURE);
                }
            } catch (Exception e) {
                Timber.e(e);
            }

        });
        imageView3.setOnClickListener(v -> {
            if (myDialog.isShowing()) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

    //To open Gallery
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }//openGallery() close

    //To take a picture from Camera
    private void takePicture(int requestImage) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "MyPicture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "Photo taken on " + System.currentTimeMillis());
        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, requestImage);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        Bitmap bitmap;
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    String filePath = BaseClass.getMimeType(getActivity(), data.getData());
                    if (filePath.equalsIgnoreCase("png") || filePath.equalsIgnoreCase("jpeg") ||
                            filePath.contains("jpg")) {
                        if (myDialog.isShowing()) {
                            myDialog.dismiss();
                        }
                        imageUri = data.getData();
                        if (imageUri != null)
                            try {
                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                                saveImage2(bitmap, imageUri);
                            } catch (IOException e) {
                                Timber.e(e);
                            }
                    } else {
                        GlobalUtils.showToast(getActivity(), "Please Upload file of Image Format");
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:
                if (myDialog.isShowing()) {
                    myDialog.dismiss();
                }
                if (resultCode == RESULT_OK) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);

                        saveImage2(bitmap, imageUri);
                    } catch (IOException e) {
                        Timber.e(e);
                    }
                }
                break;
        }//switch close
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void saveImage2(Bitmap bitmap, Uri imageUri) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        RealPathUtils filePathHelper = new RealPathUtils();

        String filePath;
        if (Build.VERSION.SDK_INT >= 23) {
            if (filePathHelper.getPathnew(imageUri, getActivity()) != null) {
                filePath = filePathHelper.getPathnew(imageUri, getActivity()).toLowerCase();
            } else {
                filePath = filePathHelper.getFilePathFromURI(imageUri, getActivity()).toLowerCase();
            }
        } else {
            filePath = filePathHelper.getPath(imageUri, getActivity()).toLowerCase();
        }
        s_file_name = getFileName(imageUri);
        imageFile = new File(filePath);


        if (setPhoto == 1) {
            //image box1
            //byte_array_one[1]=aftercropbyteArray;
            filePaths.add(imageFile.toString());
            iv_sell_images_one.setImageURI(imageUri);
            iv_plus.setOnClickListener((View v) -> {
                iv_sell_images_two.setVisibility(View.VISIBLE);
                // do something here
            });


        } else if (setPhoto == 2) {
            iv_sell_images_two.setImageURI(imageUri);
            filePaths.add(imageFile.toString());
            iv_plus.setOnClickListener((View v) -> {
                iv_sell_images_three.setVisibility(View.VISIBLE);
                // do something here
            });


        } else if (setPhoto == 3) {
            iv_plus.setVisibility(View.GONE);
            filePaths.add(imageFile.toString());
            //image box3
            iv_sell_images_three.setImageURI(imageUri);


        }

    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            try (Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    @OnClick({R.id.iv_sell_images_one, R.id.iv_sell_images_two, R.id.iv_sell_images_three, R.id.submit_button})
    public void onClick(View view) {

        if (view.getId() == R.id.iv_sell_images_one) {
            setPhoto = 1;
            uploadFilesCamera();
        } else if (view.getId() == R.id.iv_sell_images_two) {
            setPhoto = 2;
            uploadFilesCamera();

        } else if (view.getId() == R.id.iv_sell_images_three) {
            setPhoto = 3;
            uploadFilesCamera();
        } else if (view.getId() == R.id.submit_button) {
            if (GlobalUtils.isNetworkAvailable(getActivity())) {
                if (validation()) {
                    sellStatus();
                }
            }
        }
    }

    private boolean validation() {
        productTitle = et_title.getText().toString();
        productPrice = et_price.getText().toString();
        productDescription = et_product_description.getText().toString();
        if(GlobalUtils.isNullOrEmpty(categoryId)){
            GlobalUtils.showToast(getActivity(),"Please enter Category");
            return false;
        }else if(filePaths.isEmpty()){
            GlobalUtils.showToast(getActivity(), "Please Upload Images");
            return false;
        }else  if (GlobalUtils.isNullOrEmpty(productTitle)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.error_product_title));
            return false;
        } else if (GlobalUtils.isNullOrEmpty(productPrice)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.error_product_price));
            return false;
        } else if (GlobalUtils.isNullOrEmpty(productDescription)) {
            GlobalUtils.showToast(getActivity(), getString(R.string.error_product_description));
            return false;
        } else if (tv_category.getText().toString().trim().equalsIgnoreCase("")) {
            GlobalUtils.showToast(getActivity(), "Please Enter Category");
            return false;
        }

        return true;
    }

    public byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /* ignored for PNG */, bos);
        byte[] bitmapdata = bos.toByteArray();
        return bitmapdata;
    }

    private void sellStatus() {
        String collegeId = HookUpPrefrences.getCollegeId(getActivity());
        try {
            MultipartBody.Part college_id = null;
            MultipartBody.Part category_id = null;
            MultipartBody.Part product_title = null;
            MultipartBody.Part product_price = null;
            MultipartBody.Part product_description = null;
            MultipartBody.Part[] bodyimages = new MultipartBody.Part[filePaths.size()];

            if (!categoryId.isEmpty()) {
                category_id = MultipartBody.Part.createFormData("category_id", categoryId);
            }
            if (!collegeId.isEmpty()) {
                college_id = MultipartBody.Part.createFormData("college_id", collegeId);
            }
            if (!productTitle.isEmpty()) {
                product_title = MultipartBody.Part.createFormData("title", productTitle);
            }
            if (!productPrice.isEmpty()) {
                product_price = MultipartBody.Part.createFormData("price", productPrice);
            }
            if (!productDescription.isEmpty()) {
                product_description = MultipartBody.Part.createFormData("description", productDescription);
            }
            for (int i = 0; i < filePaths.size(); i++) {
                File file = new File(filePaths.get(i));
                RequestBody requestBackAddress = RequestBody.create(MediaType.parse("image/*"), file);
                bodyimages[i] = MultipartBody.Part.createFormData("image", file.getName(), requestBackAddress);
            }
            new WebServiceHandler().callSellStatus(this, HookUpPrefrences.getAuthorization(getActivity()), category_id, college_id, product_title, product_price, product_description, bodyimages);
            progress.ShowProgressDialog();
            requestType = 2;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnSuccess(Object object) {
        progress.CancelProgressDialog();
        if (requestType == 1 && object instanceof CategoryResposne) {
            CategoryResposne categoryResposne = (CategoryResposne) object;
            int ack = categoryResposne.getAck();
            if (ack == 1) {
                List<CategotyDetail> listcategory = categoryResposne.getData();
                if (listcategory != null && listcategory.size() > 0) {
                    for (int i = 0; i < listcategory.size(); i++) {
                        myStateList.add(listcategory.get(i).getCategory_name());
                    }
                    showCategoryList();
                } else {
                    myStateList.add("Data Unavailable");
                }


            }
        } else if (requestType == 2 && object instanceof SellResponse) {
            SellResponse response = (SellResponse) object;
            int ack = response.getAck();
            String message = response.getMessage();
            if (ack == 1) {
                GlobalUtils.showToastSuccess(getActivity(), message);
                Fragment recentFrag = new NewHomeFragment();
                Activity activity = getActivity();
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).loadFragment(recentFrag, false);
                } else {
                    replaceFragment(recentFrag, recentFrag.getClass().getSimpleName());
                }


            }
        }
    }

    private void showCategoryList() {
        final CharSequence[] cs = myStateList.toArray(new CharSequence[myStateList.size()]);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Category:").setItems(cs,
                (dialog, which) -> tv_category.setText(cs[which]));
        AlertDialog dialog = builder.create();
        dialog.show();
        builder.setSingleChoiceItems(cs, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                categoryId = (String.valueOf(which));
                tv_category.setText(myStateList.get(which));
                dialog.dismiss();

            }

        }).create();
//        builder.setSingleChoiceItems(myStateList, "Desc", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which)
//            {
//                long itemId = myStateList.get(se);
//
//                //do whatever you need with the ID in the DB
//
//                dialog.dismiss();
//
//
//            }
//        });


    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void onErrorHandling(Response response) {
        progress.CancelProgressDialog();
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(getActivity(), errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        progress.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(),"connection Failed");
    }

    @OnClick(R.id.iv_back)
    public void onBackPressed() {
        getActivity().onBackPressed();
    }
}
