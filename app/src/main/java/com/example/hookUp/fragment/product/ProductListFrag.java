package com.example.hookUp.fragment.product;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.adapter.ViewMoreAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.buy.BuyDetailsProductFragment;
import com.example.hookUp.fragment.home.NewHomeFragment;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.model.bean.buyResponse.BuyResponse;
import com.example.hookUp.model.bean.checksum.ChecksumRepsonse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;
import timber.log.Timber;

import static com.example.hookUp.fragment.buy.BuyDetailsProductFragment.BUNDLE_KEY_DETAILS;
import static com.example.hookUp.utils.Config.CALLBACK_URL;
import static com.example.hookUp.utils.Config.CHANNEL_ID;
import static com.example.hookUp.utils.Config.INDUSTRY_TYPE_ID;
import static com.example.hookUp.utils.Config.MID;
import static com.example.hookUp.utils.Config.WEBSITE;

public class ProductListFrag extends Fragment implements WebServiceResponse, ViewMoreAdapter.OnClickListener, PaytmPaymentTransactionCallback {
    @BindView(R.id.rv_browse_list)
    RecyclerView rv_browse_list;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_no_order)
    TextView tv_no_order;
    ViewMoreAdapter viewMoreAdpter;
    private String browse;
    private GlobalUtils globalUtils;
    private int RequestType = 1;
    private MyDialog myDialog;
    private ViewMoreAdapter adapter;
    private String amount = "", product_id = "", trans_id = "";
    private String resstatus, paytmBankName, paytmtxnResponse, paytmorderid, paytmtMode, paytmtTxnId, paytmtxnMid, paytmtCurency, paytmtGateway, paytmtxnDate, paytmResMsg;
    private String paytmtxnid, paytmaddamount, paytmpaymentid;
    private String currentFrag = "";
    public static final String BUNDLE_CATEGORY_ID = "id";
    public static final String BUNDLE_CATEGORY_NAME = "name";
    String category_id = "", category_name = "";
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.activity_product_list,container,false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }

    public void init() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BUNDLE_CATEGORY_ID)) {
            category_id = bundle.getString(BUNDLE_CATEGORY_ID);
        }
        if (bundle != null && bundle.containsKey(BUNDLE_CATEGORY_NAME)) {
            category_name = bundle.getString(BUNDLE_CATEGORY_NAME);
        }
        tv_name.setText(category_name);

        globalUtils = new GlobalUtils();
        myDialog = new MyDialog(getActivity());

        adapter = new ViewMoreAdapter(getActivity(), this);
        rv_browse_list.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_browse_list.setLayoutManager(linearLayoutManager);
        rv_browse_list.setAdapter(adapter);


    }

    @Override
    public void onResume() {
        super.onResume();
        getList();
    }

    private void getList() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            String userId = HookUpPrefrences.getUserId(getActivity());
            String college_id = HookUpPrefrences.getCollegeId(getActivity());
            String auth = HookUpPrefrences.getAuthorization(getActivity());
            new WebServiceHandler().getBuyList(this, auth, category_id, college_id);
            RequestType = 1;
            myDialog.ShowProgressDialog();

        }
    }

    private void goToProductDetail(BuyProducts contest) {
        Fragment buyDetailFrag = new BuyDetailsProductFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_DETAILS, new Gson().toJson(contest));
        buyDetailFrag.setArguments(bundle);
        Activity activity = getActivity();
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).loadMainFragment(buyDetailFrag, false);
        } else {
            replaceFragment(buyDetailFrag, buyDetailFrag.getClass().getSimpleName());
        }

    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        if (RequestType == 1 && object instanceof BuyResponse) {
            BuyResponse response = (BuyResponse) object;
            int ack = response.getAck();
            if (ack == 1) {
                List<BuyProducts> list_product = response.getBuyProductsList();
                if (list_product != null && list_product.size() > 0) {
                    adapter.update(list_product);
                    tv_no_order.setVisibility(View.GONE);
                } else {
                    tv_no_order.setVisibility(View.VISIBLE);
                }
            }
        }
        if (RequestType == 2 && object instanceof ChecksumRepsonse) {
            ChecksumRepsonse repsonse = (ChecksumRepsonse) object;
            int ack = repsonse.getAck();
            if (ack == 1) {
                String user_id = repsonse.getData().getUser_id();
                String order_id = repsonse.getData().getOrder_id();
                String amount = repsonse.getData().getAmount();
                String checksum = repsonse.getData().getChecksum();
                trans_id = repsonse.getData().getTrans_id();
                paytmhit(user_id, order_id, amount, checksum);
            }
        } else if (RequestType == 3 && object instanceof ChecksumRepsonse) {
            ChecksumRepsonse repsonse = (ChecksumRepsonse) object;
            int ack = repsonse.getAck();
            if (ack == 1) {
                GlobalUtils.showToastSuccess(getActivity(), repsonse.getMessage());
                Fragment homeFragment = new NewHomeFragment();
                Activity activity = getActivity();
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).loadFragment(homeFragment, true);
                }
            }
        }
    }

    private void paytmhit(String user_id, String order_id, String amount, String checksum) {
        try {
            HashMap<String, String> paramMap = new HashMap<>();
            paramMap.put("MID", MID);
            // Key in your staging and production MID available in your dashboard
            paramMap.put("ORDER_ID", order_id);
            paramMap.put("CUST_ID", user_id);
            paramMap.put("CHANNEL_ID", CHANNEL_ID);
            paramMap.put("TXN_AMOUNT", amount);
            paramMap.put("WEBSITE", WEBSITE);
            paramMap.put("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);
            paramMap.put("CALLBACK_URL", CALLBACK_URL + order_id);
            paramMap.put("CHECKSUMHASH", checksum);
            Log.e("PAYTMPARAM", paramMap.toString());
            PaytmOrder Order = new PaytmOrder(paramMap);

            // For Staging environment:
            PaytmPGService Service = PaytmPGService.getStagingService();
            Service.initialize(Order, null);
            Service.startPaymentTransaction(getActivity(), true, true, this);

            //  For Production environment:
            // PaytmPGService Service = PaytmPGService.getProductionService();
//            if (!BuildConfig.DEBUG) {
//                PaytmPGService Service = PaytmPGService.getProductionService();
//                Service.initialize(Order, null);
//                Service.startPaymentTransaction(getActivity(), true, true, this);
//            } else {
//                PaytmPGService Service = PaytmPGService.getStagingService();
//                Service.initialize(Order, null);
//                Service.startPaymentTransaction(getActivity(), true, true, this);
//            }

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e.toString());
        }
    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(), "connection Failed");

    }

    @Override
    public void onClick(ViewMoreAdapter adapter, int position) {
        BuyProducts contest = adapter.get(position);
        goToProductDetail(contest);
    }

    @Override
    public void onEditClick(ViewMoreAdapter adapter, int position) {
        BuyProducts details = adapter.get(position);
        String product_id = details.getId();
        String amount = details.getPrice();
        addToCart(product_id, amount);


    }

    private void addToCart(String product_id, String amount) {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            map.put("amount", amount);
            map.put("currency", "1");
            map.put("product_id", product_id);
            new WebServiceHandler().generateCheckSum(this, HookUpPrefrences.getAuthorization(getActivity()), map);
            myDialog.ShowProgressDialog();
            RequestType = 2;
        }
    }

    @Override
    public void onTransactionResponse(Bundle inResponse) {
        Log.e("PAYTM", inResponse.toString());
        JSONObject json = null;
        try {
            resstatus = inResponse.getString("STATUS");
            paytmorderid = inResponse.getString("ORDERID");
            paytmBankName = inResponse.getString("BANKNAME");
            paytmtxnid = inResponse.getString("TXNID");
            paytmtxnResponse = inResponse.getString("RESPCODE");
            paytmtMode = inResponse.getString("PAYMENTMODE");
            paytmtTxnId = inResponse.getString("BANKTXNID");
            paytmtCurency = inResponse.getString("CURRENCY");
            paytmtGateway = inResponse.getString("GATEWAYNAME");
            paytmtxnDate = inResponse.getString("TXNDATE");
            paytmaddamount = inResponse.getString("TXNAMOUNT");
            paytmpaymentid = inResponse.getString("CHECKSUMHASH");
            paytmResMsg = inResponse.getString("RESPMSG");
            hitsucess();

        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    private void hitsucess() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            map.put("STATUS", resstatus);
            map.put("CHECKSUMHASH", paytmpaymentid);
            map.put("ORDERID", paytmorderid);
            map.put("TXNAMOUNT", paytmaddamount);
            map.put("TXNDATE", paytmtxnDate);
            map.put("product_id", product_id);
            map.put("trans_id", trans_id);
            map.put("TXNID", paytmtxnid);
            map.put("PAYMENTMODE", paytmtMode);
            map.put("BANKTXNID", paytmtTxnId);
            map.put("CURRENCY", paytmtCurency);
            new WebServiceHandler().callSucessOrder(this, HookUpPrefrences.getAuthorization(getActivity()), map);
            myDialog.ShowProgressDialog();
            RequestType = 3;
        }
    }

    @Override
    public void networkNotAvailable() {

    }

    @Override
    public void clientAuthenticationFailed(String inErrorMessage) {

    }

    @Override
    public void someUIErrorOccurred(String inErrorMessage) {

    }

    @Override
    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {

    }

    @Override
    public void onBackPressedCancelTransaction() {

    }

    @Override
    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {

    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.iv_back)
    public void onBack() {
        getActivity().onBackPressed();
    }
}
