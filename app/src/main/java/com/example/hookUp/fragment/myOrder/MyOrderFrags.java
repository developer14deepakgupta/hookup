package com.example.hookUp.fragment.myOrder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.fragment.myAds.MyAddsContestFragment;
import com.example.hookUp.fragment.myAds.MyAdsFragmentAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.hookUp.utils.Constant.tabs.ACTIVE;
import static com.example.hookUp.utils.Constant.tabs.PROCESS;
import static com.example.hookUp.utils.Constant.tabs.SOLD;

public class MyOrderFrags extends Fragment {
    @BindView(R.id.tabs_settlement)
    TabLayout tabLayout;
    @BindView(R.id.pah_my_contest)
    ViewPager viewPager;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_order_tab__frag, container, false);
        ButterKnife.bind(this, view);
        intialize();
        return view;
    }

    private void intialize() {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_order_process)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab__order_active)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_reject)));
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        rv_adds.setLayoutManager(linearLayoutManager);
        List<MyOrderFragment> fragments = new ArrayList<>();
        fragments.add(MyOrderFragment.newInstance(PROCESS));
        fragments.add(MyOrderFragment.newInstance(ACTIVE));
        fragments.add(MyOrderFragment.newInstance(SOLD));
        MyorderTabAdapter adapter = new MyorderTabAdapter(getActivity(), getChildFragmentManager());
        adapter.update(fragments);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
    @OnClick(R.id.iv_back)
    public void onBackPressed(){
        getActivity().onBackPressed();
    }
}
