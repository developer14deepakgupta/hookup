package com.example.hookUp.fragment.buy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.utils.Constant;
import com.example.hookUp.utils.DateUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuyDetailAdapter extends RecyclerView.Adapter<BuyDetailAdapter.ContestViewHolder> {
    private Context context;
    private OnClickListener listener;
    private List<BuyProducts> list_product = new ArrayList<>();

    public BuyDetailAdapter(Context context) {
        this.context = context;
        this.list_product = list_product;
    }

    @NonNull
    @Override
    public ContestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.buy_adapter_list_item, parent, false);
        return new ContestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContestViewHolder holder, int position) {
        BuyProducts buyProducts = list_product.get(position);
        if (buyProducts.getId() != null)
            holder.tv_add_id.setText(buyProducts.getId());
        if (buyProducts.getTitle() != null)
            holder.tv_book_title.setText(buyProducts.getTitle());
        if (buyProducts.getImage_a() != null)
            ImageLoaderUtils.load("http://13.126.248.29:1221/" + buyProducts.getImage_a(), holder.iv_book_image);
        if (buyProducts.getIs_active() == 1) {
            holder.tv_status.setText(context.getString(R.string.label_live));
        }
        String date = buyProducts.getCreatedAt();
        String[] abc = date.split("T");
        String new_date = abc[0].trim();
        holder.tv_match_date.setText(new_date);
        if (buyProducts.getPrice() != null)
            holder.tv_book_price.setText(buyProducts.getPrice());
        holder.setOnClickListener(position1 -> {
            if (listener != null)
                listener.onClick(BuyDetailAdapter.this, position1);
        });

    }

    @Override
    public int getItemCount() {
        return list_product.size();
    }

    public BuyProducts get(int position) {
        return list_product.get(position);
    }

    public void update(List<BuyProducts> products) {
        this.list_product.clear();
        this.list_product.addAll(products);
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onClick(BuyDetailAdapter adapter, int position);
    }

    public static class ContestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_book_image)
        ImageView iv_book_image;
        @BindView(R.id.tv_book_title)
        TextView tv_book_title;
        @BindView(R.id.tv_status)
        TextView tv_status;
        @BindView(R.id.tv_match_date)
        TextView tv_match_date;
        @BindView(R.id.tv_book_price)
        TextView tv_book_price;
        @BindView(R.id.tv_add_id)
        TextView tv_add_id;
        private OnClickListener listener;

        public ContestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onClick(getAdapterPosition());
        }


        public void setOnClickListener(OnClickListener listener) {
            this.listener = listener;
        }

        public interface OnClickListener {
            void onClick(int position);
        }
    }
}
