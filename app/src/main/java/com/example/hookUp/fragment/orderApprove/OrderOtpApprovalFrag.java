package com.example.hookUp.fragment.orderApprove;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.myAds.MyAdsFrag;
import com.example.hookUp.model.bean.MyOrderList;
import com.example.hookUp.model.bean.SellResponse;
import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.example.hookUp.fragment.buy.BuyDetailsProductFragment.BUNDLE_KEY_DETAILS;


public class OrderOtpApprovalFrag extends Fragment implements WebServiceResponse {
    public static final String BUNDLE_KEY_CONTEST_TYPE = "contestType";
    @BindView(R.id.et_otp1_digit)
    EditText et_otp;
    private MyDialog myDialog;
    private String phoneOtp = "";
    private MyAddDocs docs;
    private String currentFrag;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.otp_order_approval, container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    private void initialize() {
        myDialog = new MyDialog(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BUNDLE_KEY_DETAILS)) {
            String json = bundle.getString(BUNDLE_KEY_DETAILS);
            docs = new Gson().fromJson(json, MyAddDocs.class);

        }
    }

    @OnClick({R.id.btn_confirm, R.id.iv_back})
    public void onClick(View view) {
        if (view.getId() == R.id.btn_confirm) {
            hitOtp();
        } else if (view.getId() == R.id.iv_back) {
            getActivity().onBackPressed();
        }
    }

    private void hitOtp() {
        String auth = HookUpPrefrences.getAuthorization(getActivity());
        String product_id = docs.getId();
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            if (validation()) {
                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product_id);
                map.put("otp", phoneOtp);
                new WebServiceHandler().callOtpVerfication(this, auth, map);
                myDialog.ShowProgressDialog();
            }

        }
    }

    private boolean validation() {
        phoneOtp = et_otp.getText().toString();
        if (GlobalUtils.isNullOrEmpty(phoneOtp)) {
            GlobalUtils.showToast(getActivity(), "Please enter valid otp");
            return false;
        }
        return true;
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        SellResponse response = (SellResponse) object;
        int ack = response.getAck();
        String message = response.getMessage();
        if (ack == 1) {
            GlobalUtils.showToast(getActivity(), message);
            Fragment homeFragment = new MyAdsFrag();
            currentFrag = "Home";
            Activity activity = getActivity();
            if (activity instanceof HomeActivity) {
                ((HomeActivity) activity).loadMainFragment(homeFragment, true);
            } else {
                replaceFragment(homeFragment, currentFrag);
            }
        } else if (ack == 0) {
            GlobalUtils.showToast(getActivity(), message);

        }
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(), response.toString());

    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(), "connection Failed");
    }
}
