package com.example.hookUp.fragment.buy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.adapter.BannerPagerAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.home.NewHomeFragment;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.model.bean.checksum.ChecksumRepsonse;
import com.example.hookUp.utils.CirclePageIndicator;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.ImageLoaderUtils;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;
import timber.log.Timber;

import static com.example.hookUp.utils.Config.CALLBACK_URL;
import static com.example.hookUp.utils.Config.CHANNEL_ID;
import static com.example.hookUp.utils.Config.INDUSTRY_TYPE_ID;
import static com.example.hookUp.utils.Config.MID;
import static com.example.hookUp.utils.Config.WEBSITE;
import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class BuyDetailsProductFragment extends Fragment implements WebServiceResponse, PaytmPaymentTransactionCallback {
    @BindView(R.id.tv_book_name)
    TextView tv_book_name;
    @BindView(R.id.tv_match_date)
    TextView tv_match_date;
    @BindView(R.id.tv_match_reward)
    TextView tv_match_reward;
    @BindView(R.id.tv_description)
    TextView tv_description;

    private ArrayList<BuyProducts> list_product = new ArrayList<>();
    private String paytmMobile, paytmEmail;
    private MyDialog myDialog;
    private String paytmtxnid, paytmaddamount, paytmpaymentid;
    int count = 0;
    public static final String BUNDLE_KEY_DETAILS = "details";
    private BuyProducts details;
    private String amount = "", product_id = "", trans_id = "";
    private String resstatus, paytmBankName, paytmtxnResponse, paytmorderid, paytmtMode, paytmtTxnId, paytmtxnMid, paytmtCurency, paytmtGateway, paytmtxnDate, paytmResMsg;
    int requestCall = 0;
    private View view;
    private String currentFrag;
    private int currentPage = 0;
    private BannerPagerAdapter adapter;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.dfh_banner)
    FrameLayout bannerFrameLayout;
    List<String> image_list=new ArrayList<>();
    private String image_a, image_b, image_c;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_buy_details, container, false);
        ButterKnife.bind(this, view);
        init();
        offlineBannerRequest();
        return view;
    }
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentPage == adapter.getCount()) {
                currentPage = 0;
            }
            pager.setCurrentItem(currentPage++, true);
            handler.postDelayed(this, 3000);
        }
    };
    private void offlineBannerRequest() {
        adapter = new BannerPagerAdapter(getActivity(), image_list);
        // Binds the Adapter to the ViewPager
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        //Set circle indicator radius
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(3 * density);
        // Auto start of viewpager
        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });

    }

    public void init() {
        myDialog = new MyDialog(getActivity());
        paytmMobile = HookUpPrefrences.getUserMobile(getActivity());
        paytmEmail = HookUpPrefrences.getUserEmail(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BUNDLE_KEY_DETAILS)) {
            String json = bundle.getString(BUNDLE_KEY_DETAILS);
            details = new Gson().fromJson(json, BuyProducts.class);
            if (details != null) {
                product_id = details.getId();
                amount = details.getPrice();
                list_product.add(details);
            }

            setData(count);
        }

    }

    private void setData(int count) {

        tv_book_name.setText(list_product.get(count).getTitle());
        String date = list_product.get(count).getCreatedAt();
        String[] abc = date.split("T");
        String new_date = abc[0].trim();
        tv_match_date.setText(new_date);
        tv_match_reward.setText(list_product.get(count).getPrice());
        tv_description.setText(list_product.get(count).getDescription());
        if (list_product.get(count).getImage_a() != null) {
            image_a = list_product.get(count).getImage_a();
            image_list.add(image_a);
        }
        if (list_product.get(count).getImage_b() != null) {
            image_b = list_product.get(count).getImage_b();
            image_list.add(image_b);
        }
        if (list_product.get(count).getImage_c() != null) {
            image_c = list_product.get(count).getImage_c();
            image_list.add(image_c);
        }

          }



    @OnClick({R.id.iv_back, R.id.btn_buy})
    public void onBack(View view) {
        if (view.getId() == R.id.iv_back) {
            getActivity().onBackPressed();
        } else if (view.getId() == R.id.btn_buy) {
            addtocart();
        }
    }

    private void addtocart() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            map.put("amount", amount);
            map.put("currency_id", "1");
            map.put("product_id", product_id);
                new WebServiceHandler().generateCheckSum(this, HookUpPrefrences.getAuthorization(getActivity()), map);
            myDialog.ShowProgressDialog();
            requestCall = 1;
        }
    }


    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        if (requestCall == 1 && object instanceof ChecksumRepsonse) {
            ChecksumRepsonse repsonse = (ChecksumRepsonse) object;
            int ack = repsonse.getAck();
            if (ack == 1) {
                String user_id = repsonse.getData().getUser_id();
                String order_id = repsonse.getData().getOrder_id();
                String amount = repsonse.getData().getAmount();
                String checksum = repsonse.getData().getChecksum();
                trans_id = repsonse.getData().getTrans_id();
                paytmhit(user_id, order_id, amount, checksum);
            }
        } else if (requestCall == 2 && object instanceof ChecksumRepsonse) {
            ChecksumRepsonse repsonse = (ChecksumRepsonse) object;
            int ack = repsonse.getAck();
            if (ack == 1) {
                Fragment homeFrag = new NewHomeFragment();
                Activity activity = getActivity();
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).loadMainFragment(homeFrag, false);
                } else {
                    replaceFragment(homeFrag, homeFrag.getClass().getSimpleName());
                }
                GlobalUtils.showToastSuccess(getActivity(), repsonse.getMessage());
            }
        }


    }

    private void paytmhit(String user_id, String order_id, String amount, String checksum) {
        try {
            HashMap<String, String> paramMap = new HashMap<>();
            paramMap.put("MID", MID);
            // Key in your staging and production MID available in your dashboard
            paramMap.put("ORDER_ID", order_id);
            paramMap.put("CUST_ID", user_id);
            //  paramMap.put("MOBILE_NO", paytmMobile);
            //    paramMap.put("EMAIL", paytmEmail);
            paramMap.put("CHANNEL_ID", CHANNEL_ID);
            paramMap.put("TXN_AMOUNT", amount);
            paramMap.put("WEBSITE", WEBSITE);
            paramMap.put("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);
            paramMap.put("CALLBACK_URL", CALLBACK_URL + order_id);
            paramMap.put("CHECKSUMHASH", checksum);
            Log.e("PAYTMPARAM", paramMap.toString());
            PaytmOrder Order = new PaytmOrder(paramMap);

            // For Staging environment:
            PaytmPGService Service = PaytmPGService.getStagingService();
            Service.initialize(Order, null);
            Service.startPaymentTransaction(getActivity(), true, true, this);

            //  For Production environment:
            // PaytmPGService Service = PaytmPGService.getProductionService();
//            if (!BuildConfig.DEBUG) {
//                PaytmPGService Service = PaytmPGService.getProductionService();
//                Service.initialize(Order, null);
//                Service.startPaymentTransaction(getActivity(), true, true, getActivity());
//            } else {
//                PaytmPGService Service = PaytmPGService.getStagingService();
//                Service.initialize(Order, null);
//                Service.startPaymentTransaction(getActivity(), true, true, getActivity());
//            }

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e.toString());
        }
    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(getActivity(), errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(),"connection Failed");
    }

    @Override
    public void onTransactionResponse(Bundle inResponse) {
        Log.e("PAYTM", inResponse.toString());
        JSONObject json = null;
        try {
            resstatus = inResponse.getString("STATUS");
            paytmorderid = inResponse.getString("ORDERID");
            paytmBankName = inResponse.getString("BANKNAME");
            paytmtxnid = inResponse.getString("TXNID");
            paytmtxnResponse = inResponse.getString("RESPCODE");
            paytmtMode = inResponse.getString("PAYMENTMODE");
            paytmtTxnId = inResponse.getString("BANKTXNID");
            paytmtCurency = inResponse.getString("CURRENCY");
            paytmtGateway = inResponse.getString("GATEWAYNAME");
            paytmtxnDate = inResponse.getString("TXNDATE");
            paytmaddamount = inResponse.getString("TXNAMOUNT");
            paytmpaymentid = inResponse.getString("CHECKSUMHASH");
            paytmResMsg = inResponse.getString("RESPMSG");
            if (resstatus.equalsIgnoreCase("TXN_SUCCESS")) {
                hitsucess();
            } else {
                GlobalUtils.showToast(getActivity(), "Your transaction has been Failed");
            }


        } catch (
                Exception e) {
            e.printStackTrace();
        }


    }

    private void hitsucess() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            HashMap<String, String> map = new HashMap<>();
            map.put("STATUS", resstatus);
            map.put("CHECKSUMHASH", paytmpaymentid);
            map.put("ORDERID", paytmorderid);
            map.put("TXNAMOUNT", paytmaddamount);
            map.put("TXNDATE", paytmtxnDate);
            map.put("product_id", product_id);
            map.put("trans_id", trans_id);
            map.put("TXNID", paytmtxnid);
            map.put("PAYMENTMODE", paytmtMode);
            map.put("BANKTXNID", paytmtTxnId);
            map.put("CURRENCY", paytmtCurency);
            new WebServiceHandler().callSucessOrder(this, HookUpPrefrences.getAuthorization(getActivity()), map);
            myDialog.ShowProgressDialog();
            requestCall = 2;
        }
    }

    @Override
    public void networkNotAvailable() {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void clientAuthenticationFailed(String inErrorMessage) {


    }

    @Override
    public void someUIErrorOccurred(String inErrorMessage) {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        myDialog.CancelProgressDialog();
    }

    @Override
    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
        myDialog.CancelProgressDialog();
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
       fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }
}
