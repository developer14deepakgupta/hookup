package com.example.hookUp.fragment.buy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.model.bean.buyResponse.BuyResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.example.hookUp.fragment.buy.BuyDetailsProductFragment.BUNDLE_KEY_DETAILS;

public class BuyFragment extends Fragment implements WebServiceResponse {
    @BindView(R.id.rv_contests)
    RecyclerView rv_contests;
    private MyDialog myDialog;
    private BuyDetailAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buy_fragment, container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    private void initialize() {
        myDialog = new MyDialog(getActivity());
        adapter = new BuyDetailAdapter(getActivity());
        rv_contests.setAdapter(adapter);
        adapter.setOnClickListener((adapter, position) -> {
            BuyProducts contest = adapter.get(position);
            goToProductDetail(contest);
        });

    }

    private void goToProductDetail(BuyProducts contest) {
        Fragment buyDetailFrag = new BuyDetailsProductFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_DETAILS, new Gson().toJson(contest));
        buyDetailFrag.setArguments(bundle);
        Activity activity = getActivity();
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).loadMainFragment(buyDetailFrag, false);
        } else {
            replaceFragment(buyDetailFrag, buyDetailFrag.getClass().getSimpleName());
        }
    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        getBuyList();
    }

    private void getBuyList() {
        if (GlobalUtils.isNetworkAvailable(getActivity())) {
            String userId = HookUpPrefrences.getUserId(getActivity());
            String auth = HookUpPrefrences.getAuthorization(getActivity());
            String college_id = HookUpPrefrences.getCollegeId(getActivity());
            new WebServiceHandler().getBuyallList(this, auth, college_id);
            myDialog.ShowProgressDialog();

        }
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        BuyResponse response = (BuyResponse) object;
        int ack = response.getAck();
        if (ack == 1) {
            List<BuyProducts> list_product = response.getBuyProductsList();
            if (list_product != null && list_product.size() > 0) {
                adapter.update(list_product);
            }

        }
    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(getActivity(), errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(),"connection Failed");
    }

    @OnClick(R.id.iv_back)
    public void onBackPressed() {
        getActivity().onBackPressed();
    }
}
