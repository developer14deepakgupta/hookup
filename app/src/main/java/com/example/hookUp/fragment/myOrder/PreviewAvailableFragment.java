package com.example.hookUp.fragment.myOrder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.adapter.BannerPagerAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.MyOrderList;
import com.example.hookUp.model.bean.OrderPreview;
import com.example.hookUp.utils.CirclePageIndicator;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.ImageLoaderUtils;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.example.hookUp.fragment.buy.BuyDetailsProductFragment.BUNDLE_KEY_DETAILS;
import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class PreviewAvailableFragment extends Fragment implements WebServiceResponse {
    @BindView(R.id.tv_book_name)
    TextView tv_book_name;
    @BindView(R.id.tv_product_rs)
    TextView tv_product_rs;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_match_date)
    TextView tv_match_date;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.dfh_banner)
    FrameLayout bannerFrameLayout;
    String product_id = "", auth, message = "", currentFrag;
    private View view;
    private MyDialog myDialog;
    private MyOrderList details;
    private Dialog mydiaDialog;
    private List<String> banner_list = new ArrayList<>();
    int requestType = 1;
    private int currentPage = 0;
    private BannerPagerAdapter adapter;
    private String image_a, image_b, image_c;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_order_preview, container, false);
        ButterKnife.bind(this, view);
        inIt();
        offlineBannerRequest();
        return view;
    }

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentPage == adapter.getCount()) {
                currentPage = 0;
            }
            pager.setCurrentItem(currentPage++, true);
            handler.postDelayed(this, 3000);
        }
    };

    private void offlineBannerRequest() {
        adapter = new BannerPagerAdapter(getActivity(), banner_list);
        // Binds the Adapter to the ViewPager
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        //Set circle indicator radius
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(3 * density);
        // Auto start of viewpager
        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });


    }

    private void inIt() {
        myDialog = new MyDialog(getActivity());
        auth = HookUpPrefrences.getAuthorization(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BUNDLE_KEY_DETAILS)) {
            String json = bundle.getString(BUNDLE_KEY_DETAILS);
            details = new Gson().fromJson(json, MyOrderList.class);
            if (details != null) {
                product_id = details.getProduct_id();
                tv_book_name.setText(details.getSale().getTitle());
                tv_product_rs.setText(details.getSale().getPrice());
                tv_description.setText(details.getSale().getDescription());
                if (details.getSale().getImage_a() != null) {
                    image_a = details.getSale().getImage_a();
                    banner_list.add(image_a);
                }
                if (details.getSale().getImage_b() != null) {
                    image_b = details.getSale().getImage_b();
                    banner_list.add(image_b);
                }
                if (details.getSale().getImage_c() != null) {
                    image_c = details.getSale().getImage_c();
                    banner_list.add(image_c);
                }


                for (int i = 0; i < details.getSale().getTransactions().size(); i++) {
                    tv_match_date.setText(details.getSale().getTransactions().get(i).getTransaction_date());

                }


            }

        }
    }

    @OnClick({R.id.btn_approve, R.id.btn_decline, R.id.iv_back})
    public void onBack(View view) {
        if (view.getId() == R.id.btn_approve) {
            if (GlobalUtils.isNetworkAvailable(getActivity())) {
                new WebServiceHandler().getorderPreviewActive(this, auth, product_id, "1");
            }
        } else if (view.getId() == R.id.btn_decline) {
            showdialogue();
        } else if (view.getId() == R.id.iv_back) {
            getActivity().onBackPressed();
        }
//        } else if (view.getId() == R.id.iv_share) {
//            onReferNEarnClick();
//
//        } else if (view.getId() == R.id.iv_delete) {
//            popupMenu(view);
//
//
//        }
    }

    private void popupMenu(View view) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater().inflate(R.menu.add_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.delete) {
                    delteAdd();
                }

                return true;
            }
        });
        popup.show();
    }

    private void delteAdd() {
        new WebServiceHandler().getDeltedSale(this, auth, product_id);
        myDialog.ShowProgressDialog();
        requestType = 2;

    }

    private void showdialogue() {
        mydiaDialog = new Dialog(getActivity());
        mydiaDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mydiaDialog.setCancelable(false);
        mydiaDialog.setContentView(R.layout.delete_order_layout);
        if (mydiaDialog.getWindow() != null) {
            mydiaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            EditText etMessage = mydiaDialog.findViewById(R.id.et_message);
            Button btn_cross_btn = mydiaDialog.findViewById(R.id.btn_cross_btn);
            Button btn_delete = mydiaDialog.findViewById(R.id.btn_delete);
            Button btn_cancel = mydiaDialog.findViewById(R.id.btn_cancel);
            btn_cross_btn.setOnClickListener(v -> {
                mydiaDialog.dismiss();
            });
            btn_delete.setOnClickListener(v -> {
                mydiaDialog.dismiss();
                if (createValidation(etMessage)) {
                    hitdelete(etMessage.getText().toString());
                }

            });
            btn_cancel.setOnClickListener(v -> {
                mydiaDialog.dismiss();

            });

        }
        mydiaDialog.show();
    }

    private void hitdelete(String message) {
        new WebServiceHandler().getorderPreviewdeclined(this, auth, product_id, "0", message);
        myDialog.ShowProgressDialog();
        requestType = 1;
    }

    private boolean createValidation(EditText etMessage) {
        message = etMessage.getText().toString();
        if (GlobalUtils.isNullOrEmpty(etMessage.getText().toString())) {
            etMessage.setError("Message Empty");
            return false;
        }
        return true;
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        if (requestType == 1 && object instanceof OrderPreview) {
            OrderPreview response = (OrderPreview) object;
            String message = response.getMessage();
            GlobalUtils.showToast(getActivity(), message);
            Fragment frag = new MyOrderFrags();
            Activity activity = getActivity();
            if (activity instanceof HomeActivity) {
                ((HomeActivity) activity).loadMainFragment(frag, false);
            } else {
                replaceFragment(frag, frag.getClass().getSimpleName());
            }
        } else if (requestType == 2 && object instanceof OrderPreview) {
            OrderPreview response = (OrderPreview) object;
            String message = response.getMessage();
            GlobalUtils.showToast(getActivity(), message);
            Fragment frag = new MyOrderFrags();
            Activity activity = getActivity();
            if (activity instanceof HomeActivity) {
                ((HomeActivity) activity).loadMainFragment(frag, false);
            } else {
                replaceFragment(frag, frag.getClass().getSimpleName());
            }
        }

    }

    @SuppressLint("WrongConstant")
    public void replaceFragment(Fragment fragment, String backstack_name) {
        currentFrag = backstack_name;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, backstack_name);
        fragmentTransaction.addToBackStack(backstack_name);
        fragmentTransaction.commit();
    }

    @Override
    public void onErrorHandling(Response response) {
        GlobalUtils.showToast(getActivity(), response.toString());
        myDialog.CancelProgressDialog();

    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(getActivity(), "connection Failed");
    }

    private void onReferNEarnClick() {
        final String appPackageName = getActivity().getApplication().getPackageName();
        String shareBody = "Download " + getString(R.string.app_name) + " From :  " + "http://play.google.com/store/apps/details?id=" + appPackageName;
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.app_name)));
    }
}
