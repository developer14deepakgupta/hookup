package com.example.hookUp.utils.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class MicrosoftTaileButton extends Button {
    public MicrosoftTaileButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Microsoft_taile.ttf");
        setTypeface(tf);
    }
}
