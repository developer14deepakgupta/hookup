package com.example.hookUp.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

public class DateUtils {
    private static long SECOND_IN_MILLI = 1000;
    private static long MINUTE_IN_MILLI = SECOND_IN_MILLI * 60;
    private static long HOUR_IN_MILLI = MINUTE_IN_MILLI * 60;
    private static long DAY_IN_MILLI = HOUR_IN_MILLI * 24;

    private static String printDiff(Date startDate, Date endDate) {
        long difference = endDate.getTime() - startDate.getTime();

        long elapsedDays = difference / DAY_IN_MILLI;
        difference = difference % DAY_IN_MILLI;

        long elapsedHours = difference / HOUR_IN_MILLI;
        difference = difference % HOUR_IN_MILLI;

        long elapsedMinutes = difference / MINUTE_IN_MILLI;
        difference = difference % MINUTE_IN_MILLI;

        if (elapsedDays == 0) {
            long elapsedSeconds = difference / SECOND_IN_MILLI;
            return elapsedHours + "h " + elapsedMinutes + "m " + elapsedSeconds + "s";
        } else {
            return elapsedDays + "d " + elapsedHours + "h " + elapsedMinutes + "m";
        }
    }

    private static String printPlayDiff(Date startDate, Date endDate) {
        long difference = endDate.getTime() - startDate.getTime() - 11 * SECOND_IN_MILLI;

        long elapsedDays = difference / DAY_IN_MILLI;
        difference = difference % DAY_IN_MILLI;

        long elapsedHours = difference / HOUR_IN_MILLI;
        difference = difference % HOUR_IN_MILLI;

        long elapsedMinutes = difference / MINUTE_IN_MILLI;
        difference = difference % MINUTE_IN_MILLI;

        if (elapsedDays == 0) {
            long elapsedSeconds = difference / SECOND_IN_MILLI;
            return elapsedHours + "h " + elapsedMinutes + "m " + elapsedSeconds + "s";
        } else {
            return elapsedDays + "d " + elapsedHours + "h " + elapsedMinutes + "m";
        }
    }

    public static String getMatchTiming(String startTime) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(Constant.DateTimeFormat.SERVER_DATE_TIME, Locale.getDefault());
            Date matchStart = df.parse(startTime);
            Date currentDate = Calendar.getInstance().getTime();
            if (matchStart.before(currentDate)) {
                return "0d 0h 0m";
            } else {
                return printDiff(currentDate, matchStart);
            }
        } catch (Exception e) {
            Timber.e("Error parsing the date");
            return "0d 0h 0m";
        }
    }

    public static String getContestPlayTiming(String startTime) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(Constant.DateTimeFormat.SERVER_DATE_TIME, Locale.getDefault());
            Date matchStart = df.parse(startTime);
            Date currentDate = Calendar.getInstance().getTime();
            long startTime1 = matchStart.getTime()-12000;
            long endTime1 = currentDate.getTime();
            if (startTime1<=endTime1) {
                return "0d 0h 0m";
            } else {
                return printPlayDiff(currentDate, matchStart);
            }
        } catch (Exception e) {
            Timber.e("Error parsing the date");
            return "0d 0h 0m";
        }
    }

    public static String getDate(String dateTime, String source, String target) {
        try {
            SimpleDateFormat sourceFormat = new SimpleDateFormat(source, Locale.getDefault());
            Date sourceDate = sourceFormat.parse(dateTime);
            SimpleDateFormat targetFormat = new SimpleDateFormat(target, Locale.getDefault());
            return targetFormat.format(sourceDate);
        } catch (Exception e) {
            Timber.e("Error parsing the date");
            return dateTime;
        }
    }

    public static Date getDate(String date, String format) {
        try {
            SimpleDateFormat sourceFormat = new SimpleDateFormat(format, Locale.getDefault());
            return sourceFormat.parse(date);
        } catch (Exception e) {
            Timber.e("Error parsing the date");
            return null;
        }
    }
}
