package com.example.hookUp.utils.colorDialogue;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;

import androidx.appcompat.app.AlertDialog;

import com.example.hookUp.R;
import com.example.hookUp.utils.ViewUtils;


public class AlerDilaogue {
    private Context context;
    private AlertDialog alertDialog;

    public void show() {
        alertDialog.show();
        ViewUtils.hideKeyboard(context);
    }

    public void hide() {
        if (alertDialog.isShowing()) {
            alertDialog.dismiss();
            ViewUtils.hideKeyboard(context);
        }
    }

    public interface OnBtnClickListener {
        void onYesClick();

        void onNoClick();
    }


    public AlerDilaogue(final Context context, String mMessage, String yesBtnText, String noBtnText, final OnBtnClickListener onBtnClickListener) {

        this.context = context;
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.app_name));

        // set dialog message
        alertDialogBuilder.setMessage(mMessage).setCancelable(false);


        if (!TextUtils.isEmpty(yesBtnText)) {
            alertDialogBuilder.setPositiveButton(yesBtnText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    hide();
                    if (onBtnClickListener != null) {
                        onBtnClickListener.onYesClick();
                    }
                }
            });
        }

        if (!TextUtils.isEmpty(noBtnText)) {
            alertDialogBuilder.setNegativeButton(noBtnText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    hide();
                    if (onBtnClickListener != null) {
                        onBtnClickListener.onNoClick();
                    }
                }
            });
        }


        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setWindowAnimations(R.style.AlertDialogTheme);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorAccent));
                alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorAccent));
            }
        });
    }


}
