package com.example.hookUp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class HookUpPrefrences {
    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setUserMobile(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_MOBILE, userId).apply();
    }

    public static String getUserMobile(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_MOBILE, "");
    }

    public static void setUserEmail(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_EMAIL, userId).apply();
    }

    public static String getUserEmail(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_EMAIL, "");
    }

    public static void setUserCollege(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_COLLEGE, userId).apply();
    }

    public static String getUserCollege(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_COLLEGE, "");
    }

    public static void setUserCourse(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_COURSE, userId).apply();
    }

    public static String getUserCourse(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_COURSE, "");
    }

    public static void setUserStream(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_STREAM, userId).apply();
    }

    public static String getUserStream(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_STREAM, "");
    }

    public static void setUserImage(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_IMAGE, userId).apply();
    }

    public static String getUserImage(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_IMAGE, "");
    }

    public static void setUserUniversity(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_UNIVERSITY, userId).apply();
    }

    public static String getUserUniversity(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_UNIVERSITY, "");
    }

    public static void setUserYear(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_YEAR, userId).apply();
    }

    public static String getUserYear(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_YEAR, "");
    }

    public static void setUserName(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_NAME, userId).apply();
    }

    public static String getUserName(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_NAME, "");
    }

    public static void setUserAccountNumber(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.ACCOUNT_NUMBER, userId).apply();
    }

    public static String getUserAccountNumber(Context context) {
        return getSharedPreferences(context).getString(Constant.ACCOUNT_NUMBER, "");
    }

    public static void setBankName(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.BANK_NAME, userId).apply();
    }

    public static String getBankName(Context context) {
        return getSharedPreferences(context).getString(Constant.BANK_NAME, "");
    }

    public static void setAccountType(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.ACCOUNT_TYPE, userId).apply();
    }

    public static String getAccountType(Context context) {
        return getSharedPreferences(context).getString(Constant.ACCOUNT_TYPE, "");
    }

    public static void setAccountName(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.ACCOUNT_NAME, userId).apply();
    }

    public static String getAccountName(Context context) {
        return getSharedPreferences(context).getString(Constant.ACCOUNT_NUMBER, "");
    }

    public static void setIfscCode(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.IFSC_CODE, userId).apply();
    }

    public static String getIfscCode(Context context) {
        return getSharedPreferences(context).getString(Constant.IFSC_CODE, "");
    }

    public static void setUpiId(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.UPI_ID, userId).apply();
    }

    public static String getUpiId(Context context) {
        return getSharedPreferences(context).getString(Constant.UPI_ID, "");
    }

    public static void setUserGender(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.DEALER_GENDER, userId).apply();
    }

    public static String getUserGender(Context context) {
        return getSharedPreferences(context).getString(Constant.DEALER_GENDER, "");
    }

    public static void setUserId(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.USER_ID, userId).apply();
    }

    public static String getUserId(Context context) {
        return getSharedPreferences(context).getString(Constant.USER_ID, "");
    }

    public static void setAuthorization(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.AUTHORIZATION, userId).apply();
    }

    public static String getAuthorization(Context context) {
        return getSharedPreferences(context).getString(Constant.AUTHORIZATION, "");
    }

    public static void setCollegeId(Context context, String userId) {
        getSharedPreferences(context).edit().putString(Constant.COLLEGE_ID, userId).apply();
    }

    public static String getCollegeId(Context context) {
        return getSharedPreferences(context).getString(Constant.COLLEGE_ID, "");
    }


    public static void ClearPreferences(Context context) {
        getSharedPreferences(context).edit().clear().apply();
    }

    public static void setUserLoggedIn(Context context, boolean value) {
        getSharedPreferences(context).edit().putBoolean(Constant.IS_LOGGED_IN, value).apply();
    }

    public static boolean getUserLoggedIn(Context context) {
        return getSharedPreferences(context).getBoolean(Constant.IS_LOGGED_IN, false);
    }

}
