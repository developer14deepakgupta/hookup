package com.example.hookUp.utils.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class MicrosostTaileTextView extends TextView {
    public MicrosostTaileTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Microsoft_taile.ttf");
        setTypeface(tf);
    }
}
