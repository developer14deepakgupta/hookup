package com.example.hookUp.utils;

public interface Constant {
    String DEALER_ID = "dealerId";
    String DEALER_IMAGES = "dealerImages";
    String DEALER_MOBILE = "dealerMobile";
    String DEALER_EMAIL = "dealeremail";
    String DEALER_COLLEGE = "dealercollege";
    String DEALER_UNIVERSITY = "dealeruniversity";
    String DEALER_STREAM = "dealerstream";
    String DEALER_IMAGE = "dealerimage";
    String DEALER_YEAR = "dealeryear";
    String DEALER_COURSE = "dealercourse";
    String DEALER_NAME = "dealername";
    String UPI_ID = "upiId";
    String BANK_NAME = "bankName";
    String ACCOUNT_NUMBER = "accountNumber";
    String ACCOUNT_NAME = "accountName";
    String ACCOUNT_TYPE = "accountType";
    String IFSC_CODE = "ifsccode";
    String DEALER_GENDER = "dealergender";
    String USER_ID = "userId";
    String AUTHORIZATION = "auth";
    String COLLEGE_ID = "collegeId";
    String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    String IMAGE_PATH="http://13.126.248.29:1221/";

    String IS_LOGGED_IN = "isLogedIn";
    String REFERRED_SCREEN = "referredScreen";

    // HTTP Response
    String HTTP_200 = "200";
    String HTTP_400 = "400";

    interface DateTimeFormat {

        String APP_DATE = "dd-MMM-yyyy";
        String APP_DATE_TIME = "dd-MM-yyyy hh:mma";
        String SERVER_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
        String SERVER_DATE_TIME_2 = "EEE MMM dd yyyy HH:mm:ss Z";
    }

    String MALE = "MALE";
    String FEMALE = "FEMALE";
    String TYPE_SAVING="saving";
    String TYPE_CURRENT="current";

    interface tabs{
     String ACTIVE="active";
     String PROCESS="process";
     String SOLD="sold";
    }
}
