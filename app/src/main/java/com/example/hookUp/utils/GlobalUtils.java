package com.example.hookUp.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hookUp.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GlobalUtils {
    public static final String PERSON_FULL_NAME_PATTERN = "^[\\p{L} .'-]+$";
    public static final String PERSON_FIRST_NAME_OR_LAST_NAME_PATTERN = "^[\\\\p{L} .'-]+$";


    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public static boolean isNetworkAvailable(Context activity) {
        if (activity != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            @SuppressLint("MissingPermission") NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } else
            return false;
    }
    public static boolean isValidEmail(String emailAddress) {
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    public static boolean isValidPassword(String password) {

        return password.length() < 7;
    }
    public static boolean isPasswordValid(String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);


        return matcher.matches();

    }
    public static boolean isNullOrEmpty(String str) {
        if (str != null && !str.isEmpty()) {
            return false;
        }
        return true;
    }
    public static boolean isIfscCodeValid(String code) {
        String regExp = "^[A-Z]{4}[A-Z0-9]{7}$";
        boolean isValid = false;

        if (code.length() > 0) {
            isValid = code.matches(regExp);
        }
        return isValid;
    }

    public static boolean isUpiIdValid(String code) {
        String regExp = "[a-zA-Z0-9\\.\\-]{2,256}\\@[a-zA-Z][a-zA-Z]{2,64}";
        boolean isValid = false;

        if (code.length() > 0) {
            isValid = code.matches(regExp);
        }
        return isValid;
    }


    public static boolean isValidPersonFirstOrLastName(String personFirstorLastName) {
        return Pattern.compile(PERSON_FIRST_NAME_OR_LAST_NAME_PATTERN).matcher(personFirstorLastName).matches();
    }

    public static boolean isValidPersonFullName(String personFirstorLastName) {
        return Pattern.compile(PERSON_FULL_NAME_PATTERN).matcher(personFirstorLastName).matches();
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return Patterns.PHONE.matcher(phoneNumber).matches();
    }

    /* method for mobile number validation */
    public final static Pattern MOBILE_NUMBER_PATTERN = Pattern
            .compile("^[0-9]{10}$");

    public static boolean checkMobile(String mobile) {
        try {
            mobile = mobile.replaceAll("[^0-9]", "");
            return MOBILE_NUMBER_PATTERN.matcher(mobile).matches();
        } catch (Exception exception) {
            return false;
        }
    }
    public static void showToastSuccess(Context context, String msg) {
        if (context == null || msg == null || msg.isEmpty())
            return;
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.games91_layout_custom_toast, null);
        TextView text = layout.findViewById(R.id.toast_title);
        text.setText(msg);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

}
