package com.example.hookUp.api;

import com.example.hookUp.model.bean.BankResponse;
import com.example.hookUp.model.bean.ForgotResponse;
import com.example.hookUp.model.bean.LoginResponse.LoginRecord;
import com.example.hookUp.model.bean.MyOrderResponse;
import com.example.hookUp.model.bean.OrderPreview;
import com.example.hookUp.model.bean.SellResponse;
import com.example.hookUp.model.bean.TopCategoryProducts;
import com.example.hookUp.model.bean.buyResponse.BuyResponse;
import com.example.hookUp.model.bean.checksum.ChecksumRepsonse;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategoryResposne;
import com.example.hookUp.model.bean.finduniversityResponse.collegeResponse.CollegeResponse;
import com.example.hookUp.model.bean.finduniversityResponse.courseResponse.CourseResponse;
import com.example.hookUp.model.bean.finduniversityResponse.registerResponse.RegistrationResponse;
import com.example.hookUp.model.bean.finduniversityResponse.streamResponse.StreamReponse;
import com.example.hookUp.model.bean.finduniversityResponse.universityResponse.UniversityResponse;
import com.example.hookUp.model.bean.finduniversityResponse.yearResponse.YearResponse;
import com.example.hookUp.model.bean.myAdds.MyAddResponse;
import com.example.hookUp.model.bean.query.QueryResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestInterface {

    @POST("/login")
    @FormUrlEncoded
    Call<LoginRecord> callLoginService(@FieldMap Map<String, String> param);

    @POST("/register")
    @FormUrlEncoded
    Call<RegistrationResponse> callRegsiterService(@FieldMap Map<String, String> param);

    @POST("/addAcademic")
    @FormUrlEncoded
    Call<RegistrationResponse> callAddAcademicService(@Header("Authorization") String authHeader, @FieldMap Map<String, String> param);

    @POST("/addBank")
    @FormUrlEncoded
    Call<BankResponse> callAddBankService(@Header("Authorization") String authHeader, @FieldMap Map<String, String> param);

    @POST("/forgotPassword")
    @FormUrlEncoded
    Call<ForgotResponse> callForgotService(@Field("email") String email);

    @POST("/recoverForgotPassword")
    @FormUrlEncoded
    Call<ForgotResponse> callRememberService(@Header("Authorization") String authHeader, @FieldMap Map<String, String> param);

    @POST("/OtpVerification")
    @FormUrlEncoded
    Call<SellResponse> callOtpVerficationService(@Header("Authorization") String authHeader, @FieldMap Map<String, String> param);

    @POST("/contact")
    @FormUrlEncoded
    Call<ForgotResponse> callContactUsService(@Header("Authorization") String authHeader, @FieldMap Map<String, String> param);

    @POST("/verifyPayment")
    @FormUrlEncoded
    Call<ChecksumRepsonse> sucessOrder(@Header("Authorization") String authHeader, @FieldMap Map<String, String> param);

    @GET("/findUniversity/{university_id}")
    Call<UniversityResponse> callUniversityService(@Path(value = "university_id") String id);

    @GET("/getQuery")
    Call<QueryResponse> callGetQueryService();

    @GET("/findCategory/{id}")
    Call<CategoryResposne> callCategoryService(@Path(value = "id") String id);

    @GET("/findCollege/{university_id}")
    Call<CollegeResponse> callCollegeService(@Path(value = "university_id") String id);

    @GET("/findCourse/{course_id}")
    Call<CourseResponse> callCourseService(@Path(value = "course_id") String id);

    @GET("/findStream/{course_id}")
    Call<StreamReponse> callStreamService(@Path(value = "course_id") String id);

    @GET("/findYear/{year_id}")
    Call<YearResponse> callYearService(@Path(value = "year_id") String id);

    @GET("/buy")
    Call<BuyResponse> callBuyProductService(@Header("Authorization") String authHeader, @Query("category_id") String category_id, @Query("college_id") String id);

    @GET("/buy")
    Call<BuyResponse> callBuyAllProductList(@Header("Authorization") String authHeader, @Query("college_id") String id);

    @GET("/topCategories")
    Call<TopCategoryProducts> callBuytopCategoriesList();

    @GET("/myAdds")
    Call<MyAddResponse> callMyaddActiveService(@Header("Authorization") String authHeader, @Query("is_active") String id);

    @PUT("/productStatus")
    @FormUrlEncoded
    Call<OrderPreview> callOrderApproveService(@Header("Authorization") String authHeader, @Field("product_id") String product_id, @Field("is_approved") String id);

    @PUT("/productStatus")
    @FormUrlEncoded
    Call<OrderPreview> callOrderDeclinedService(@Header("Authorization") String authHeader,@Field("product_id") String product_id,@Field("is_approved") String id,@Field("msg") String message);

    @HTTP(method = "DELETE", path = "/deleteSale", hasBody = true)
    @FormUrlEncoded
    Call<OrderPreview> callOrderDeleted(@Header("Authorization") String authHeader, @Field("product_id") String product_id);

    @GET("/myAdds")
    Call<MyAddResponse> callMyaddProcessService(@Header("Authorization") String authHeader, @Query("in_process") String id);

    @GET("/myAdds")
    Call<MyAddResponse> callMyaddSoldService(@Header("Authorization") String authHeader, @Query("is_sold") String id);

    @POST("/getCheckSum")
    @FormUrlEncoded
    Call<ChecksumRepsonse> callGenerateCheksum(@Header("Authorization") String authHeader, @FieldMap Map<String, String> params);

    @GET("/myOrders")
    Call<MyOrderResponse> callMyAllOrder(@Header("Authorization") String authHeader);

    @GET("/myOrders")
    Call<MyOrderResponse> callMyApproveOrder(@Header("Authorization") String authHeader, @Query("is_approved") String id);

    @GET("/myOrders")
    Call<MyOrderResponse> callMyRejectOrder(@Header("Authorization") String authHeader, @Query("is_rejected") String id);

    @Multipart
    @POST("/sale")
    Call<SellResponse> uploadMultiFile(@Header("Authorization") String authHeader, @Part MultipartBody.Part bodycategory, @Part MultipartBody.Part bodycollegid, @Part MultipartBody.Part bodytitle,
                                       @Part MultipartBody.Part bodyprice, @Part MultipartBody.Part bodydecritpition, @Part MultipartBody.Part[] bodyimages);
}
