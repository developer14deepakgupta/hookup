package com.example.hookUp.api;

import retrofit2.Response;

public interface WebServiceResponse {
    void OnSuccess(Object object);

    void onErrorHandling(Response response);

    void OnFailure();
}
