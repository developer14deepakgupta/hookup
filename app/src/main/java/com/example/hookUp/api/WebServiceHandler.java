package com.example.hookUp.api;


import android.util.Log;

import com.example.hookUp.model.bean.BankResponse;
import com.example.hookUp.model.bean.ForgotResponse;
import com.example.hookUp.model.bean.LoginResponse.LoginRecord;
import com.example.hookUp.model.bean.MyOrderResponse;
import com.example.hookUp.model.bean.OrderPreview;
import com.example.hookUp.model.bean.SellResponse;
import com.example.hookUp.model.bean.TopCategoryProducts;
import com.example.hookUp.model.bean.buyResponse.BuyResponse;
import com.example.hookUp.model.bean.checksum.ChecksumRepsonse;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategoryResposne;
import com.example.hookUp.model.bean.finduniversityResponse.collegeResponse.CollegeResponse;
import com.example.hookUp.model.bean.finduniversityResponse.courseResponse.CourseResponse;
import com.example.hookUp.model.bean.finduniversityResponse.registerResponse.RegistrationResponse;
import com.example.hookUp.model.bean.finduniversityResponse.streamResponse.StreamReponse;
import com.example.hookUp.model.bean.finduniversityResponse.universityResponse.UniversityResponse;
import com.example.hookUp.model.bean.finduniversityResponse.yearResponse.YearResponse;
import com.example.hookUp.model.bean.myAdds.MyAddResponse;
import com.example.hookUp.model.bean.query.QueryResponse;
import com.example.hookUp.utils.GlobalUtils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class WebServiceHandler {
    private RestInterface restInterface;
    private WebServiceResponse webServiceResponse;
    GlobalUtils globalUtils;
    RetrofitSinglton single;
    // TestSingleton restInterfaceTest;

    public WebServiceHandler() {
        // restInterface = TestSingleton.getTestClient().create(RestInterface.class);
        restInterface = RetrofitSinglton.getClient().create(RestInterface.class);
//        if (restInterfaceTest == null)
//            restInterfaceTest = TestSingleton.getTestClient().create(RestInterface.class);
    }

    public void callLoginAPI(final WebServiceResponse webServiceResponse, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<LoginRecord> call = restInterface.callLoginService(regData);
        call.enqueue(new Callback<LoginRecord>() {
            @Override
            public void onResponse(Call<LoginRecord> call, Response<LoginRecord> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }


            }

            @Override
            public void onFailure(Call<LoginRecord> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    public void callRegisterAPI(final WebServiceResponse webServiceResponse, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<RegistrationResponse> call = restInterface.callRegsiterService(regData);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //add edutcation Details
    public void callAddEducationAPI(final WebServiceResponse webServiceResponse, String token, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<RegistrationResponse> call = restInterface.callAddAcademicService(token, regData);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    } //add edutcation Details

    public void callAddBankAPI(final WebServiceResponse webServiceResponse, String token, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<BankResponse> call = restInterface.callAddBankService(token, regData);
        call.enqueue(new Callback<BankResponse>() {
            @Override
            public void onResponse(Call<BankResponse> call, Response<BankResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<BankResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //Forgot password
    public void callForgotAPI(final WebServiceResponse webServiceResponse, final String regData) {
        this.webServiceResponse = webServiceResponse;
        Call<ForgotResponse> call = restInterface.callForgotService(regData);
        call.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //Forgot password
    public void callOtpVerfication(final WebServiceResponse webServiceResponse, String token, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<SellResponse> call = restInterface.callOtpVerficationService(token, regData);
        call.enqueue(new Callback<SellResponse>() {
            @Override
            public void onResponse(Call<SellResponse> call, Response<SellResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<SellResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //Remember password password
    public void callRememberAPI(final WebServiceResponse webServiceResponse, String header, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<ForgotResponse> call = restInterface.callRememberService(header, regData);
        call.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    // Contact Us

    public void callcontactUsAPI(final WebServiceResponse webServiceResponse, String header, final Map<String, String> regData) {
        this.webServiceResponse = webServiceResponse;
        Call<ForgotResponse> call = restInterface.callContactUsService(header, regData);
        call.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }
    // Contact Us

    public void callgetQueryAPI(final WebServiceResponse webServiceResponse) {
        this.webServiceResponse = webServiceResponse;
        Call<QueryResponse> call = restInterface.callGetQueryService();
        call.enqueue(new Callback<QueryResponse>() {
            @Override
            public void onResponse(Call<QueryResponse> call, Response<QueryResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<QueryResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    public void getUniversityList(final WebServiceResponse webServiceResponse, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<UniversityResponse> call = restInterface.callUniversityService(id);
        call.enqueue(new Callback<UniversityResponse>() {
            @Override
            public void onResponse(Call<UniversityResponse> call, Response<UniversityResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<UniversityResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    // college id
    public void getCollegeList(final WebServiceResponse webServiceResponse, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<CollegeResponse> call = restInterface.callCollegeService(id);
        call.enqueue(new Callback<CollegeResponse>() {
            @Override
            public void onResponse(Call<CollegeResponse> call, Response<CollegeResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<CollegeResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //Cousre List
    public void getCourseList(final WebServiceResponse webServiceResponse, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<CourseResponse> call = restInterface.callCourseService(id);
        call.enqueue(new Callback<CourseResponse>() {
            @Override
            public void onResponse(Call<CourseResponse> call, Response<CourseResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<CourseResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //StreamList
    public void getStreamList(final WebServiceResponse webServiceResponse, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<StreamReponse> call = restInterface.callStreamService(id);
        call.enqueue(new Callback<StreamReponse>() {
            @Override
            public void onResponse(Call<StreamReponse> call, Response<StreamReponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<StreamReponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //YearList
    public void getYearList(final WebServiceResponse webServiceResponse, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<YearResponse> call = restInterface.callYearService(id);
        call.enqueue(new Callback<YearResponse>() {
            @Override
            public void onResponse(Call<YearResponse> call, Response<YearResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<YearResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //CategoryList
    public void getCategoryList(final WebServiceResponse webServiceResponse, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<CategoryResposne> call = restInterface.callCategoryService(id);
        call.enqueue(new Callback<CategoryResposne>() {
            @Override
            public void onResponse(Call<CategoryResposne> call, Response<CategoryResposne> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<CategoryResposne> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //buy product
    public void getBuyList(final WebServiceResponse webServiceResponse, String auth, String id, String college_id) {
        this.webServiceResponse = webServiceResponse;
        Call<BuyResponse> call = restInterface.callBuyProductService(auth, id, college_id);
        call.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }//buy product

    // Top Selling List

    public void getBuyTopList(final WebServiceResponse webServiceResponse) {
        this.webServiceResponse = webServiceResponse;
        Call<TopCategoryProducts> call = restInterface.callBuytopCategoriesList();
        call.enqueue(new Callback<TopCategoryProducts>() {
            @Override
            public void onResponse(Call<TopCategoryProducts> call, Response<TopCategoryProducts> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<TopCategoryProducts> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }//buy product

    //buy all  product
    public void getBuyallList(final WebServiceResponse webServiceResponse, String auth, String college_id) {
        this.webServiceResponse = webServiceResponse;
        Call<BuyResponse> call = restInterface.callBuyAllProductList(auth, college_id);
        call.enqueue(new Callback<BuyResponse>() {
            @Override
            public void onResponse(Call<BuyResponse> call, Response<BuyResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<BuyResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //buy all  product
    public void getOrderallList(final WebServiceResponse webServiceResponse, String auth) {
        this.webServiceResponse = webServiceResponse;
        Call<MyOrderResponse> call = restInterface.callMyAllOrder(auth);
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //order active  product
    public void getOrderActiveList(final WebServiceResponse webServiceResponse, String auth, String college_id) {
        this.webServiceResponse = webServiceResponse;
        Call<MyOrderResponse> call = restInterface.callMyApproveOrder(auth, college_id);
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //order rejected  product
    public void getOrderRejectedList(final WebServiceResponse webServiceResponse, String auth, String college_id) {
        this.webServiceResponse = webServiceResponse;
        Call<MyOrderResponse> call = restInterface.callMyRejectOrder(auth, college_id);
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    // MyaddActiveList
    public void getAddActiveList(final WebServiceResponse webServiceResponse, String auth, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<MyAddResponse> call = restInterface.callMyaddActiveService(auth, id);
        call.enqueue(new Callback<MyAddResponse>() {
            @Override
            public void onResponse(Call<MyAddResponse> call, Response<MyAddResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyAddResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }
    // MyaddActiveList

    public void getorderPreviewActive(final WebServiceResponse webServiceResponse, String auth,String product_id, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<OrderPreview> call = restInterface.callOrderApproveService(auth,product_id, id);
        call.enqueue(new Callback<OrderPreview>() {
            @Override
            public void onResponse(Call<OrderPreview> call, Response<OrderPreview> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<OrderPreview> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }// MyaddActiveList

    public void getorderPreviewdeclined(final WebServiceResponse webServiceResponse, String auth, String product_id, String id,String message) {
        this.webServiceResponse = webServiceResponse;
        Call<OrderPreview> call = restInterface.callOrderDeclinedService(auth,product_id, id,message);
        call.enqueue(new Callback<OrderPreview>() {
            @Override
            public void onResponse(Call<OrderPreview> call, Response<OrderPreview> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<OrderPreview> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    public void getDeltedSale(final WebServiceResponse webServiceResponse, String auth, String product_id) {
        this.webServiceResponse = webServiceResponse;
        Call<OrderPreview> call = restInterface.callOrderDeleted(auth, product_id);
        call.enqueue(new Callback<OrderPreview>() {
            @Override
            public void onResponse(Call<OrderPreview> call, Response<OrderPreview> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<OrderPreview> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    // MyaddProcessList
    public void getProcessList(final WebServiceResponse webServiceResponse, String auth, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<MyAddResponse> call = restInterface.callMyaddProcessService(auth, id);
        call.enqueue(new Callback<MyAddResponse>() {
            @Override
            public void onResponse(Call<MyAddResponse> call, Response<MyAddResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyAddResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    // MyaddSoldList
    public void getSoldList(final WebServiceResponse webServiceResponse, String auth, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<MyAddResponse> call = restInterface.callMyaddSoldService(auth, id);
        call.enqueue(new Callback<MyAddResponse>() {
            @Override
            public void onResponse(Call<MyAddResponse> call, Response<MyAddResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyAddResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //buy product
    public void callSellStatus(final WebServiceResponse webServiceResponse, String auth, MultipartBody.Part category_id, MultipartBody.Part college_id, MultipartBody.Part product_title, MultipartBody.Part product_price, MultipartBody.Part product_description,
                               MultipartBody.Part[] images) {
        this.webServiceResponse = webServiceResponse;
        Call<SellResponse> call = restInterface.uploadMultiFile(auth, category_id, college_id, product_title, product_price, product_description, images);
        call.enqueue(new Callback<SellResponse>() {
            @Override
            public void onResponse(Call<SellResponse> call, Response<SellResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<SellResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    //buy product
    public void callOrderList(final WebServiceResponse webServiceResponse, String auth, String id) {
        this.webServiceResponse = webServiceResponse;
        Call<MyOrderResponse> call = restInterface.callMyAllOrder(auth);
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    } //buy product

    public void generateCheckSum(final WebServiceResponse webServiceResponse, String token, final Map<String, String> map) {
        this.webServiceResponse = webServiceResponse;
        Call<ChecksumRepsonse> call = restInterface.callGenerateCheksum(token, map);
        call.enqueue(new Callback<ChecksumRepsonse>() {
            @Override
            public void onResponse(Call<ChecksumRepsonse> call, Response<ChecksumRepsonse> response) {
                if (response.code() == 400 || response.code() == 404 || response.code() == 500) {
                    webServiceResponse.onErrorHandling(response);
                } else if (response.code() == 200) {
                    webServiceResponse.OnSuccess(response.body());
                } else {
                    webServiceResponse.onErrorHandling(response);
                }
            }

            @Override
            public void onFailure(Call<ChecksumRepsonse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });
    }

    public void callSucessOrder(final WebServiceResponse webServiceResponse, String token, HashMap<String, String> regData) {
        Call<ChecksumRepsonse> call = restInterface.sucessOrder(token, regData);
        call.enqueue(new Callback<ChecksumRepsonse>() {
            @Override
            public void onResponse(Call<ChecksumRepsonse> call, Response<ChecksumRepsonse> response) {
                if (response.code() == 400 || response.code() == 401 || response.code() == 404 || response.code() == 402) {
                    webServiceResponse.onErrorHandling(response);
                } else {
                    webServiceResponse.OnSuccess(response.body());
                    Log.e("TAG", response.toString());
                }
            }

            @Override
            public void onFailure(Call<ChecksumRepsonse> call, Throwable t) {
                webServiceResponse.OnFailure();
                Log.e(getClass().getSimpleName(), t.toString());
                Log.e("TAG", t.toString());
                Timber.e(t.toString());
            }
        });

    }
}
