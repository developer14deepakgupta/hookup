package com.example.hookUp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class BannerPagerAdapter extends PagerAdapter {
    private Context context;
    private List<String> banners;

    public BannerPagerAdapter(Context context,List<String> banner_lsit) {
        this.context = context;
        this.banners = new ArrayList<>();
        this.banners=banner_lsit;
    }

    @Override
    public int getCount() {
        return banners.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View sliderView = LayoutInflater.from(context).inflate(R.layout.slider_item, container, false);
        ImageView flagImageView = sliderView.findViewById(R.id.flag);
        ImageLoaderUtils.load(IMAGE_PATH+banners.get(position), flagImageView);
        container.addView(sliderView);
        return sliderView;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
