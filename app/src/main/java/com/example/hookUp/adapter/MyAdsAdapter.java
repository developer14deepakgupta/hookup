package com.example.hookUp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.fragment.buy.BuyDetailAdapter;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.example.hookUp.utils.Constant;
import com.example.hookUp.utils.DateUtils;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.ContestViewHolder> {
    private Context context;
    private List<MyAddDocs> list_product = new ArrayList<>();
    private OnClickListener listener;
    private String value;


    public MyAdsAdapter(Context context, List<MyAddDocs> list_product, String value) {
        this.context = context;
        this.list_product = list_product;
        this.value = value;
    }

    @NonNull
    @Override
    public MyAdsAdapter.ContestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myadd_list_item, parent, false);
        return new MyAdsAdapter.ContestViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull MyAdsAdapter.ContestViewHolder holder, int position) {
        MyAddDocs buyProducts = list_product.get(position);

        if (buyProducts.getTitle() != null)
            holder.tv_title_name.setText(buyProducts.getTitle());
        if (buyProducts.getImage_a() != null)
            ImageLoaderUtils.load(IMAGE_PATH + buyProducts.getImage_a(), holder.iv_book_image);
//        if (buyProducts.getIs_active() == 1) {
//            holder.tv_active.setText(context.getString(R.string.label_active));
//        } else {
//            holder.tv_active.setText(context.getString(R.string.label_inactive));
//        }
        holder.tv_active.setText(value);
        if (buyProducts.getDescription() != null) {
            holder.tv_descirption_name.setText(buyProducts.getDescription());
        }
        if (buyProducts.getPrice() != null)
            holder.tv_price_name.setText(buyProducts.getPrice());

        if (value.equalsIgnoreCase("Active")) {
            holder.iv_share.setVisibility(View.VISIBLE);
            holder.iv_delete.setVisibility(View.VISIBLE);
            holder.setOnClickListener(new ContestViewHolder.OnClickListener() {
                @Override
                public void onShareClick(int position) {
                    if (listener != null) {
                        listener.onShareClick(MyAdsAdapter.this, position);
                    }
                }

                @Override
                public void onDelteClick(int position) {
                    if (listener != null) {
                        listener.onDeleteClick(MyAdsAdapter.this, position);
                    }
                }


            });
        }
    }

    @Override
    public int getItemCount() {
        return list_product.size();
    }

    public MyAddDocs get(int position) {
        return list_product.get(position);
    }

    public void update(List<MyAddDocs> products) {
        this.list_product.clear();
        this.list_product.addAll(products);
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onShareClick(MyAdsAdapter adapter, int position);

        void onDeleteClick(MyAdsAdapter adapter, int position);
    }


    public static class ContestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_pacakge_image)
        ImageView iv_book_image;
        @BindView(R.id.iv_delete)
        ImageView iv_delete;
        @BindView(R.id.iv_share)
        ImageView iv_share;
        @BindView(R.id.tv_title_name)
        TextView tv_title_name;
        @BindView(R.id.tv_price_name)
        TextView tv_price_name;
        @BindView(R.id.tv_descirption_name)
        TextView tv_descirption_name;
        @BindView(R.id.tv_package_session)
        TextView tv_active;
        private OnClickListener listener;

        public ContestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            iv_share.setOnClickListener(this);
            iv_delete.setOnClickListener(this);
        }

        public void setOnClickListener(OnClickListener listener) {
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.iv_delete) {
                listener.onDelteClick(getAdapterPosition());
            } else if (v.getId() == R.id.iv_share) {
                listener.onShareClick(getAdapterPosition());
            }
        }
        public interface OnClickListener {
            void onShareClick(int position);

            void onDelteClick(int position);
        }
    }
}
