package com.example.hookUp.adapter.horizontalAdatper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.RecycleListener;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategotyDetail;
import com.example.hookUp.utils.ImageLoaderUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.EventListHolder> {
    private Context context;
    private RecycleListener clickListener;
    private List<CategotyDetail> category_list;

    public CategoryAdapter(Context context, RecycleListener clickListener) {
        this.category_list = new ArrayList<>();
        this.clickListener = clickListener;
        this.context = context;
    }

    @Override
    public EventListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new EventListHolder(view);
    }

    public void setSelectedItem(int position) {
        category_list.get(position).setSelected(true);
        for (int i = 0; i < category_list.size(); i++) {
            if (i != position) {
                category_list.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(EventListHolder holder, int position) {
        CategotyDetail chip = category_list.get(position);
        String active = category_list.get(position).getIs_active();
        holder.tv_catogry.setText(category_list.get(position).getCategory_name());
        if(chip.getImage()!=null)
          Picasso.get().load("http://13.126.248.29:1221/"+chip.getImage()).resize(90, 90).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return category_list.size();
    }

    public void update(List<CategotyDetail> chips) {
        this.category_list.clear();
        this.category_list.addAll(chips);
        notifyDataSetChanged();
    }

    public CategotyDetail get(int position) {
        return category_list.get(position);
    }

    public CategotyDetail getSelected() {
        for (CategotyDetail d : category_list) {
            if (d.isSelected())
                return d;
        }
        return null;
    }


    public class EventListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.grid_image)
        ImageView imageView;
        @BindView(R.id.tv_catogry)
        TextView tv_catogry;

        public EventListHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.itemClicked(v, getAdapterPosition());
        }
    }
}
