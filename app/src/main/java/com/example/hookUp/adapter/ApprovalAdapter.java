package com.example.hookUp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.ContestViewHolder> {
    private Context context;
    private List<MyAddDocs> list_product;
    private OnClickListener listener;

    public ApprovalAdapter(Context context) {
        this.context = context;
        this.list_product = new ArrayList<>();
    }

    @NonNull
    @Override
    public ApprovalAdapter.ContestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myadd_list_item, parent, false);
        return new ContestViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ApprovalAdapter.ContestViewHolder holder, int position) {
        MyAddDocs buyProducts = list_product.get(position);

        if (buyProducts.getTitle() != null)
            holder.tv_title_name.setText(buyProducts.getTitle());
        if (buyProducts.getImage_a() != null)
            ImageLoaderUtils.load("http://13.126.248.29:1221/" + buyProducts.getImage_a(), holder.iv_book_image);
        if (buyProducts.getIs_active() == 1) {
            holder.tv_active.setText(context.getString(R.string.label_active));
        } else {
            holder.tv_active.setText(context.getString(R.string.label_inactive));
        }
        if (buyProducts.getDescription() != null) {
            holder.tv_descirption_name.setText(buyProducts.getDescription());
        }
        if (buyProducts.getPrice() != null)
            holder.tv_price_name.setText(buyProducts.getPrice());
        holder.setOnClickListener(position1 -> {
            if (listener != null)
                listener.onClick(ApprovalAdapter.this, position1);
        });
    }

    @Override
    public int getItemCount() {
        return list_product.size();
    }

    public MyAddDocs get(int position) {
        return list_product.get(position);
    }

    public void update(List<MyAddDocs> products) {
        this.list_product.clear();
        this.list_product.addAll(products);
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onClick(ApprovalAdapter adapter, int position);
    }

    public static class ContestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_pacakge_image)
        ImageView iv_book_image;
        @BindView(R.id.tv_title_name)
        TextView tv_title_name;
        @BindView(R.id.tv_price_name)
        TextView tv_price_name;
        @BindView(R.id.tv_descirption_name)
        TextView tv_descirption_name;
        @BindView(R.id.tv_package_session)
        TextView tv_active;
        private OnClickListener listener;

        public ContestViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onClick(getAdapterPosition());

        }

        public void setOnClickListener(OnClickListener listener) {
            this.listener = listener;
        }

        public interface OnClickListener {
            void onClick(int position);
        }
    }

}
