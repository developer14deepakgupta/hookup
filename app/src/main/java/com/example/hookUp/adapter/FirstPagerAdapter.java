package com.example.hookUp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.model.bean.SliderDatum;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

public class FirstPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<SliderDatum> sliderlist=new ArrayList<>();
    SliderClickInterface sliderClickInterface;
//    public FirstPagerAdapter(HomeActivity homeActivity, List<SliderDatum> slider, Context context){
//        this.context = context;
//
//    }

    public FirstPagerAdapter(HomeActivity homeActivity, List<SliderDatum> slider, HomeActivity activity) {
        this.sliderlist=slider;
        this.context=homeActivity;
       sliderClickInterface= (SliderClickInterface) activity;
        if (context != null)
            inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return sliderlist.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View sliderView = inflater.inflate(R.layout.slideritem,container,false);
        ImageView imageView = sliderView.findViewById(R.id.ivslider);

        String image = sliderlist.get(position).getSliderImage();
        ImageLoaderUtils.load(image,imageView);

        ((ViewPager) container).addView(sliderView, position);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sliderClickInterface.bannerClick(position);
            }
        });

//        if(position==1){
//            imageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i=new Intent(context, CategoryFragment.class);
//                    context.startActivity(i);
//                }
//            });


        //}
        return sliderView;




    }
    public interface SliderClickInterface{
        void bannerClick(int pos);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
