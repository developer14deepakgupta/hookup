package com.example.hookUp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.fragment.myOrder.MyOrderAdapter;
import com.example.hookUp.model.bean.MyOrderList;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class MyTransitOrderAdapter extends RecyclerView.Adapter<MyTransitOrderAdapter.MyViewHolder> {
    private List<MyOrderList> orders;
    private Context context;
    private OnClickListener listener;

    public MyTransitOrderAdapter(Context context) {
        this.context = context;
        this.orders = new ArrayList<>();

    }

    @NonNull
    @Override
    public MyTransitOrderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list, parent, false);
        return new MyTransitOrderAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyTransitOrderAdapter.MyViewHolder holder, int position) {
        MyOrderList list = orders.get(position);
        if (orders.get(position) != null) {
            if (list.getSale() != null) {
                if (list.getSale().getTransactions() != null) {
                    for (int i = 0; i < list.getSale().getTransactions().size(); i++) {
                        if (list.getSale().getTransactions().get(i).getTransactionId() != null)
                            holder.tv_order_number.setText(list.getSale().getTransactions().get(i).getOrderId());
                        if (list.getSale().getTransactions().get(i).getTransaction_date() != null)
                            holder.tv_ordered_date.setText(list.getSale().getTransactions().get(i).getTransaction_date());
                        if (list.getSale().getTransactions().get(i).getPayment_mode() != null)
                            holder.tv_order_payment_method.setText(list.getSale().getTransactions().get(i).getPayment_mode());

                    }
                }
                if (list.getSale().getPrice() != null)
                    holder.tv_amount_paid.setText(list.getSale().getPrice());
                if (list.getSale().getImage_a() != null)
                    ImageLoaderUtils.load(IMAGE_PATH + list.getSale().getImage_a(), holder.iv_order_prod_image);
            }

        }
        holder.setOnClickListener(position1 -> {
            if (listener != null)
                listener.onClick(MyTransitOrderAdapter.this, position1);
        });
    }

    public void update(List<MyOrderList> list) {
        this.orders.clear();
        this.orders.addAll(list);
        notifyDataSetChanged();
    }

    public MyOrderList get(int position) {
        return orders.get(position);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onClick(MyTransitOrderAdapter adapter, int position);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_order_no)
        TextView tv_order_number;
        @BindView(R.id.tv_ordered_date)
        TextView tv_ordered_date;
        @BindView(R.id.tv_amount_paid)
        TextView tv_amount_paid;
        @BindView(R.id.tv_order_payment_method)
        TextView tv_order_payment_method;
        @BindView(R.id.iv_order_prod_image)
        ImageView iv_order_prod_image;
        private MyTransitOrderAdapter.MyViewHolder.OnClickListener listener;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onClick(getAdapterPosition());
        }

        public void setOnClickListener(MyTransitOrderAdapter.MyViewHolder.OnClickListener listener) {
            this.listener = listener;
        }

        public interface OnClickListener {
            void onClick(int position);
        }
    }
}
