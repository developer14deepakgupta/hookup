package com.example.hookUp.adapter.horizontalAdatper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.fragment.buy.BuyDetailAdapter;
import com.example.hookUp.model.bean.TopCategoryProducts;
import com.example.hookUp.model.bean.TopProducts;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FirstHorizontalAdapter extends RecyclerView.Adapter<FirstHorizontalAdapter.HorizontalViewHolder> {
     Context context;
    private FirstHorizontalAdapter.OnClickListener listener;
    private List<TopProducts> list_product = new ArrayList<>();


    public FirstHorizontalAdapter(Context context ) {
        this.context = context;
        this.list_product = list_product;
    }

    @Override
    public HorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontalitem, parent, false);
        HorizontalViewHolder viewHolder = new HorizontalViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalViewHolder holder, int position) {
        TopProducts buyProducts = list_product.get(position);
        if (buyProducts.getCategory_name() != null)
            holder.tvMerchant.setText(buyProducts.getCategory_name());

        holder.setOnClickListener(position1 -> {
            if (listener != null)
                listener.onClick(FirstHorizontalAdapter.this, position1);
        });
    }

    @Override
    public int getItemCount() {
        return list_product.size();
    }

    public TopProducts get(int position) {
        return list_product.get(position);
    }

    public void update(List<TopProducts> products) {
        this.list_product.clear();
        this.list_product.addAll(products);
        notifyDataSetChanged();
    }


    public void setOnClickListener(FirstHorizontalAdapter.OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onClick(FirstHorizontalAdapter adapter, int position);
    }

    public static class HorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvMerchant)
        TextView tvMerchant;
        private OnClickListener listener;
        public HorizontalViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null)
                listener.onClick(getAdapterPosition());

    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

        public interface OnClickListener {
            void onClick(int position);
        }
}

}
