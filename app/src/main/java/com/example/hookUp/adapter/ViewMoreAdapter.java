package com.example.hookUp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hookUp.R;
import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.example.hookUp.utils.Constant;
import com.example.hookUp.utils.DateUtils;
import com.example.hookUp.utils.ImageLoaderUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.hookUp.utils.Constant.IMAGE_PATH;

public class ViewMoreAdapter extends RecyclerView.Adapter<ViewMoreAdapter.MyViewHolder> {
    private Context context;
    private OnClickListener listener;
    private List<BuyProducts> list_product = new ArrayList<>();

    public ViewMoreAdapter(Context context, OnClickListener listener) {
        this.context = context;
        this.list_product = list_product;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.compounditem, parent, false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;

    }

    public BuyProducts get(int position) {
        return list_product.get(position);
    }

    public void update(List<BuyProducts> products) {
        this.list_product.clear();
        this.list_product.addAll(products);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BuyProducts buyProducts = list_product.get(position);
        if (buyProducts.getId() != null)
            holder.tv_add_id.setText(buyProducts.getId());
        if (buyProducts.getTitle() != null)
            holder.tv_book_title.setText(buyProducts.getTitle());
        if (buyProducts.getImage_a() != null)
            ImageLoaderUtils.load(IMAGE_PATH + buyProducts.getImage_a(), holder.iv_book_image);

        if (buyProducts.getPrice() != null)
            holder.tv_book_price.setText("\u20B9" + buyProducts.getPrice());
        holder.setOnClickListener(new MyViewHolder.OnClickListener() {
            @Override
            public void onClick(int position) {
                if (listener != null) {
                    listener.onClick(ViewMoreAdapter.this, position);
                }
            }

            @Override
            public void onEditClick(int position) {
                if (listener != null) {
                    listener.onEditClick(ViewMoreAdapter.this, position);
                }
            }
        });

    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public interface OnClickListener {
        void onClick(ViewMoreAdapter adapter, int position);

        void onEditClick(ViewMoreAdapter adapter, int position);
    }

    @Override
    public int getItemCount() {
        return list_product.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_book_image)
        ImageView iv_book_image;
        @BindView(R.id.tv_book_title)
        TextView tv_book_title;
        @BindView(R.id.tv_book_price)
        TextView tv_book_price;
        @BindView(R.id.tv_add_id)
        TextView tv_add_id;
        @BindView(R.id.lladdtocart)
        LinearLayout cartAddButton;
        private OnClickListener listener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            cartAddButton.setOnClickListener(this);
        }
        public void setOnClickListener(OnClickListener listener) {
            this.listener = listener;
        }
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.lladdtocart) {
                this.listener.onEditClick(getAdapterPosition());
            } else {
                this.listener.onClick(getAdapterPosition());
            }
        }



        public interface OnClickListener {
            void onClick(int position);

            void onEditClick(int position);
        }
    }

}
