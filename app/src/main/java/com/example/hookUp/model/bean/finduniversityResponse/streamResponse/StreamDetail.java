package com.example.hookUp.model.bean.finduniversityResponse.streamResponse;

import com.google.gson.annotations.SerializedName;

public class StreamDetail {
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStream_name() {
        return stream_name;
    }

    public void setStream_name(String stream_name) {
        this.stream_name = stream_name;
    }

    @SerializedName("stream_name")
    private String stream_name;
}
