package com.example.hookUp.model.bean.browseCategory;

import com.google.gson.annotations.SerializedName;

public class BrowseCategoryDetails {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String preduct_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreduct_name() {
        return preduct_name;
    }

    public void setPreduct_name(String preduct_name) {
        this.preduct_name = preduct_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @SerializedName("price")
    private String price;
}
