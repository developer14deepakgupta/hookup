package com.example.hookUp.model.bean.myAdds;

import com.google.gson.annotations.SerializedName;

public class MyAddDocs {
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("price")
    private String price;
    @SerializedName("image_a")
    private String image_a;

    public Integer getIs_active() {
        return is_active;
    }

    public void setIs_active(Integer is_active) {
        this.is_active = is_active;
    }

    public Integer getIs_sold() {
        return is_sold;
    }

    public void setIs_sold(Integer is_sold) {
        this.is_sold = is_sold;
    }

    @SerializedName("is_active")
    private Integer is_active;
    @SerializedName("is_sold")
    private Integer is_sold;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage_a() {
        return image_a;
    }

    public void setImage_a(String image_a) {
        this.image_a = image_a;
    }
}
