package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Sale {
    @SerializedName("id")
    public String id;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("price")
    public String price;
    @SerializedName("image_a")
    public String image_a;
    @SerializedName("image_b")
    public String image_b;
    @SerializedName("image_c")
    public String image_c;

    public String getImage_a() {
        return image_a;
    }

    @SerializedName("transactions")
    private List<TransactionResonse> transactions;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public List<TransactionResonse> getTransactions() {
        return transactions;
    }

    public String getImage_b() {
        return image_b;
    }

    public String getImage_c() {
        return image_c;
    }


}
