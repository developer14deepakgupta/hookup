package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class SellResponse {
    @SerializedName("ack")
    private int ack;

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("msg")
    private String message;

}
