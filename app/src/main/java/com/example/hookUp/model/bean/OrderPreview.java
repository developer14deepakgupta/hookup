package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class OrderPreview {
    @SerializedName("ack")
    private int ack;
    @SerializedName("msg")
    private String message;

    public int getAck() {
        return ack;
    }

    public String getMessage() {
        return message;
    }
}
