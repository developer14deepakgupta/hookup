package com.example.hookUp.model.bean.myAdds;

import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyAddResponse {
    @SerializedName("ack")
    private int ack;

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }


    public List<MyAddDocs> getBuyProductsList() {
        return buyProductsList;
    }

    public void setBuyProductsList(List<MyAddDocs> buyProductsList) {
        this.buyProductsList = buyProductsList;
    }

    @SerializedName("data")
    private List<MyAddDocs> buyProductsList = null;

    public String getMessage() {
        return message;
    }

    @SerializedName("message")
    private String message;

}
