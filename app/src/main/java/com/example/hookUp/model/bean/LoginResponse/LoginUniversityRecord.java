package com.example.hookUp.model.bean.LoginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginUniversityRecord {
    @SerializedName("id")
    private String id;
    @SerializedName("university_name")
    private String uninversityName;

    public String getUninversityName() {
        return uninversityName;
    }

    public void setUninversityName(String uninversityName) {
        this.uninversityName = uninversityName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
