package com.example.hookUp.model.bean.LoginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginEducationDetails {
    @SerializedName("university_id")
    private String universityId;
    @SerializedName("college_id")
    private String collegeId;
    @SerializedName("university")
    private LoginUniversityRecord loginUniversityRecord;
    @SerializedName("college")
    private LoginCollegeRecord loginCollegeRecord;
    @SerializedName("course")
    private LoginCourseRecord loginCourseRecord;
    @SerializedName("stream")
    private LoginStreamRecord loginStreamRecord;
    @SerializedName("year")
    private LoginYearRecord loginYearRecord;

    public LoginUniversityRecord getLoginUniversityRecord() {
        return loginUniversityRecord;
    }

    public void setLoginUniversityRecord(LoginUniversityRecord loginUniversityRecord) {
        this.loginUniversityRecord = loginUniversityRecord;
    }

    public LoginCollegeRecord getLoginCollegeRecord() {
        return loginCollegeRecord;
    }

    public void setLoginCollegeRecord(LoginCollegeRecord loginCollegeRecord) {
        this.loginCollegeRecord = loginCollegeRecord;
    }

    public LoginCourseRecord getLoginCourseRecord() {
        return loginCourseRecord;
    }

    public void setLoginCourseRecord(LoginCourseRecord loginCourseRecord) {
        this.loginCourseRecord = loginCourseRecord;
    }

    public LoginStreamRecord getLoginStreamRecord() {
        return loginStreamRecord;
    }

    public void setLoginStreamRecord(LoginStreamRecord loginStreamRecord) {
        this.loginStreamRecord = loginStreamRecord;
    }

    public LoginYearRecord getLoginYearRecord() {
        return loginYearRecord;
    }

    public void setLoginYearRecord(LoginYearRecord loginYearRecord) {
        this.loginYearRecord = loginYearRecord;
    }

    public String getUniversityId() {
        return universityId;
    }

    public void setUniversityId(String universityId) {
        this.universityId = universityId;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }
}
