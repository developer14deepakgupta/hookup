package com.example.hookUp.model.bean.finduniversityResponse.universityResponse;

import com.google.gson.annotations.SerializedName;

public class UniversityDetail {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    @SerializedName("id")
    private String id;
    @SerializedName("university_name")
  
    private String universityName;
    @SerializedName("is_active")
  
    private Integer isActive;
    @SerializedName("created")
  
    private String created;
    @SerializedName("modified")
  
    private String modified;

}
