package com.example.hookUp.model.bean.browseCategory;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrowseCategoryResponse {
    @SerializedName("ack")
    private int ack;
    @SerializedName("message")
    private String message;

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BrowseCategoryDetails> getBrowseData() {
        return browseData;
    }

    public void setBrowseData(List<BrowseCategoryDetails> browseData) {
        this.browseData = browseData;
    }

    @SerializedName("data")
    private List<BrowseCategoryDetails> browseData=null;
}
