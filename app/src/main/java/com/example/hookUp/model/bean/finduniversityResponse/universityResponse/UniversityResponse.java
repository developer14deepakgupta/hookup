package com.example.hookUp.model.bean.finduniversityResponse.universityResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UniversityResponse {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<UniversityDetail> data = null;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<UniversityDetail> getData() {
        return data;
    }

    public void setData(List<UniversityDetail> data) {
        this.data = data;
    }
}
