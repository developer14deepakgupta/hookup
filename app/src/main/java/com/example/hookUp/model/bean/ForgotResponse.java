package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class ForgotResponse {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;
    @SerializedName("st")
    private String st;

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
