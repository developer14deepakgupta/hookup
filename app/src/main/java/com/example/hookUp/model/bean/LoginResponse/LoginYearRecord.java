package com.example.hookUp.model.bean.LoginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginYearRecord {
    @SerializedName("id")
    private String id;

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    @SerializedName("year_name")
    private String yearName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
