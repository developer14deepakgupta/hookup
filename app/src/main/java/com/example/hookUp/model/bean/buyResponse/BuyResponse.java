package com.example.hookUp.model.bean.buyResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyResponse {
    @SerializedName("ack")
    private int ack;

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }

    public List<BuyProducts> getBuyProductsList() {
        return buyProductsList;
    }

    public void setBuyProductsList(List<BuyProducts> buyProductsList) {
        this.buyProductsList = buyProductsList;
    }

    @SerializedName("data")
    private List<BuyProducts> buyProductsList = null;
}
