package com.example.hookUp.model.bean.LoginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginCollegeRecord {
    @SerializedName("id")
    private String id;
    @SerializedName("college_name")
    private String collegeName;

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
