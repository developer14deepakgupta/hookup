package com.example.hookUp.model.bean.base;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("ack")
    private Integer ack;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @SerializedName("msg")
    private String msg;
}
