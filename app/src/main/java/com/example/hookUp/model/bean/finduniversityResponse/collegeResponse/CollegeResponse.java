package com.example.hookUp.model.bean.finduniversityResponse.collegeResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CollegeResponse {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<CollegeDetail> getData() {
        return data;
    }

    public void setData(List<CollegeDetail> data) {
        this.data = data;
    }

    @SerializedName("data")
    private List<CollegeDetail> data = null;
}
