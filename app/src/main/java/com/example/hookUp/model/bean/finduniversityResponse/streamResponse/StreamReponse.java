package com.example.hookUp.model.bean.finduniversityResponse.streamResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StreamReponse {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<StreamDetail> getData() {
        return data;
    }

    public void setData(List<StreamDetail> data) {
        this.data = data;
    }

    @SerializedName("data")
    private List<StreamDetail> data = null;
}
