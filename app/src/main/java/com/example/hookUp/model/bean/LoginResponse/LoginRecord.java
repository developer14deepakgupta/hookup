package com.example.hookUp.model.bean.LoginResponse;

import com.example.hookUp.model.bean.base.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class LoginRecord extends BaseResponse {
    @SerializedName("st")
    private String st;
    @SerializedName("user_profile")
    private LoginProfile userProfile;
    @SerializedName("education_details")
    private List<LoginEducationDetails> educationDetails = null;

    public LoginBankDetails getLoginBankDetails() {
        return loginBankDetails;
    }

    public void setLoginBankDetails(LoginBankDetails loginBankDetails) {
        this.loginBankDetails = loginBankDetails;
    }

    @SerializedName("bank_details")
    private LoginBankDetails loginBankDetails;

    public List<LoginEducationDetails> getEducationDetails() {
        return educationDetails;
    }

    public void setEducationDetails(List<LoginEducationDetails> educationDetails) {
        this.educationDetails = educationDetails;
    }

    public LoginProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(LoginProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }


}
