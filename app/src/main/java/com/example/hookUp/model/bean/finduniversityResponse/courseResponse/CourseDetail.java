package com.example.hookUp.model.bean.finduniversityResponse.courseResponse;

import com.google.gson.annotations.SerializedName;

public class CourseDetail {
    @SerializedName("id")
    private String id;
    @SerializedName("year_ids")
    private String year_ids;

    public String getYear_ids() {
        return year_ids;
    }

    public void setYear_ids(String year_ids) {
        this.year_ids = year_ids;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    @SerializedName("course_name")

    private String course_name;
}
