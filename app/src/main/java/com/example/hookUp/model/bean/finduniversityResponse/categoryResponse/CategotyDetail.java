package com.example.hookUp.model.bean.finduniversityResponse.categoryResponse;

import com.google.gson.annotations.SerializedName;

public class CategotyDetail {
    @SerializedName("id")
    private String id;
    @SerializedName("category_name")
    private String category_name;
    @SerializedName("is_active")
    private String is_active;
    @SerializedName("img_url")
    private String image;

    public String getImage() {
        return image;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }





    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }
}
