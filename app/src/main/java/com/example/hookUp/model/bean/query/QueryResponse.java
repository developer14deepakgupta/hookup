package com.example.hookUp.model.bean.query;

import com.example.hookUp.model.bean.checksum.ChecksumClass;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QueryResponse {
    @SerializedName("ack")
    private int ack;

    public List<QueryData> getData() {
        return data;
    }

    @SerializedName("msg")
    private String message;
    @SerializedName("data")
    private List<QueryData> data;

    public int getAck() {
        return ack;
    }

    public String getMessage() {
        return message;
    }


}
