package com.example.hookUp.model.bean;

import com.example.hookUp.model.bean.myAdds.MyAddDocs;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrderResponse {
    @SerializedName("ack")
    private int ack;
    @SerializedName("data")
    private List<MyOrderList> myOrderList;

    public int getAck() {
        return ack;
    }
    public void setAck(int ack) {
        this.ack = ack;
    }
    public List<MyOrderList> getBuyProductsList() {
        return myOrderList;
    }


}
