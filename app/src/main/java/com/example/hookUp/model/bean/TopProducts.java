package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class TopProducts {
    @SerializedName("category_name")
    private String category_name;

    public String getCategory_name() {
        return category_name;
    }
}
