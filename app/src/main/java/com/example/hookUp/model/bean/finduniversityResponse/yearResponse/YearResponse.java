package com.example.hookUp.model.bean.finduniversityResponse.yearResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YearResponse {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<YearDetail> getData() {
        return data;
    }

    public void setData(List<YearDetail> data) {
        this.data = data;
    }

    @SerializedName("data")
    private List<YearDetail> data = null;

}
