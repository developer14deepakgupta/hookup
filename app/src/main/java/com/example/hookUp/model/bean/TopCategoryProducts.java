package com.example.hookUp.model.bean;

import com.example.hookUp.model.bean.buyResponse.BuyProducts;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopCategoryProducts {
    @SerializedName("ack")
    private int ack;

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }

    public List<TopProducts> getBuyProductsList() {
        return topProducts;
    }

    public void setBuyProductsList(List<TopProducts> topProducts) {
        this.topProducts = topProducts;
    }

    @SerializedName("data")
    private List<TopProducts> topProducts = null;
}
