package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class MyOrderList {
    @SerializedName("user_id")
    private String id;
    @SerializedName("product_id")
    private String product_id;
    @SerializedName("sale")
    private Sale sale;

    public Sale getSale() {
        return sale;
    }

    public String getId() {
        return id;
    }

    public String getProduct_id() {
        return product_id;
    }

}
