package com.example.hookUp.model.bean.finduniversityResponse.registerResponse;

import com.google.gson.annotations.SerializedName;

public class RegistrationResponse {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
