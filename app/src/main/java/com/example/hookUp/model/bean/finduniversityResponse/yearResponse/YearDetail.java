package com.example.hookUp.model.bean.finduniversityResponse.yearResponse;

import com.google.gson.annotations.SerializedName;

public class YearDetail {
    @SerializedName("id")
    private String id;
    @SerializedName("year_name")
    private String year_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYear_name() {
        return year_name;
    }

    public void setYear_name(String year_name) {
        this.year_name = year_name;
    }
}
