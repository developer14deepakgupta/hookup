package com.example.hookUp.model.bean.base;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse {
    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;
}
