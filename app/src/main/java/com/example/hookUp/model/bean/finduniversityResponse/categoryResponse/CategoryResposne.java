package com.example.hookUp.model.bean.finduniversityResponse.categoryResponse;

import com.example.hookUp.model.bean.finduniversityResponse.collegeResponse.CollegeDetail;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResposne {
    @SerializedName("ack")
    private Integer ack;
    @SerializedName("msg")
    private String msg;

    public Integer getAck() {
        return ack;
    }

    public void setAck(Integer ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<CategotyDetail> getData() {
        return data;
    }

    public void setData(List<CategotyDetail> data) {
        this.data = data;
    }

    @SerializedName("data")
    private List<CategotyDetail> data = null;
}
