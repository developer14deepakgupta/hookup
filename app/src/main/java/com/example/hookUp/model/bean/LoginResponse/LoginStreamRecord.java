package com.example.hookUp.model.bean.LoginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginStreamRecord {
    @SerializedName("id")
    private String id;
    @SerializedName("stream_name")
    private String streamName;

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    }
