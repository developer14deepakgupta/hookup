package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class TransactionResonse {
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("response_email")
    private String responseEmail;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("transaction_date")
    private String transaction_date;

    public String getTransaction_date() {
        return transaction_date;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getResponseEmail() {
        return responseEmail;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    @SerializedName("payment_mode")
    private String payment_mode;

}
