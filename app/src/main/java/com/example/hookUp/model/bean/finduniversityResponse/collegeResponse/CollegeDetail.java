package com.example.hookUp.model.bean.finduniversityResponse.collegeResponse;

import com.google.gson.annotations.SerializedName;

public class CollegeDetail {
    @SerializedName("id")
    private String id;
    @SerializedName("college_name")
    private String college_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }
}
