package com.example.hookUp.model.bean.checksum;

import com.google.gson.annotations.SerializedName;

public class ChecksumClass {
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("order_id")
    private String order_id;
    @SerializedName("product_id")
    private String product_id;
    @SerializedName("status")
    private String status;
    @SerializedName("amount")
    private String amount;
    @SerializedName("checksum")
    private String checksum;
    @SerializedName("trans_id")
    private String trans_id;

    public String getTrans_id() {
        return trans_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getStatus() {
        return status;
    }

    public String getAmount() {
        return amount;
    }

    public String getChecksum() {
        return checksum;
    }


}
