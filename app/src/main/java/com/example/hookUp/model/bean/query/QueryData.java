package com.example.hookUp.model.bean.query;

import com.google.gson.annotations.SerializedName;

public class QueryData {
    @SerializedName("id")
    private String id;
    @SerializedName("query_msg")
    private String message;

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
