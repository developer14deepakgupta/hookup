package com.example.hookUp.model.bean.checksum;

import com.google.gson.annotations.SerializedName;

public class ChecksumRepsonse {
    @SerializedName("ack")
    private int ack;
    @SerializedName("msg")
    private String message;
    @SerializedName("data")
    private ChecksumClass data;

    public int getAck() {
        return ack;
    }

    public String getMessage() {
        return message;
    }

    public ChecksumClass getData() {
        return data;
    }
}
