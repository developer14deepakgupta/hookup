package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class BankData {
    @SerializedName("id")
    private String id;
    @SerializedName("account_no")
    private String account_no;
    @SerializedName("account_type")
    private String account_type;
    @SerializedName("ifsc_code")
    private String ifsc_code;
    @SerializedName("account_name")
    private String account_name;
    @SerializedName("bank_name")
    private String bank_name;
    @SerializedName("upi_id")
    private String upi_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_no() {
        return account_no;
    }
    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public void setIfsc_code(String ifsc_code) {
        this.ifsc_code = ifsc_code;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getUpi_id() {
        return upi_id;
    }

    public void setUpi_id(String upi_id) {
        this.upi_id = upi_id;
    }
}
