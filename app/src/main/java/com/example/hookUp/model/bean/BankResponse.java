package com.example.hookUp.model.bean;

import com.google.gson.annotations.SerializedName;

public class BankResponse {
    @SerializedName("ack")
    private int ack;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private BankData bankData;

    public BankData getBankData() {
        return bankData;
    }

    public void setBankData(BankData bankData) {
        this.bankData = bankData;
    }

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
