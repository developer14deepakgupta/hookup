package com.example.hookUp.base;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.hookUp.R;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.home.NewHomeFragment;
import com.example.hookUp.utils.GlobalUtils;


import retrofit2.Response;
import timber.log.Timber;

public class BaseFragment extends Fragment implements WebServiceResponse {
    private boolean isAlive;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        isAlive = true;
        Timber.d("Fragment name is: ${BaseFragment.class.getSimpleName()}");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }

    public boolean isAlive() {
        return isAlive;
    }


    public void replaceFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        String tagName = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container, fragment, tagName);
        fragmentTransaction.addToBackStack(tagName);
        fragmentTransaction.commit();
    }

    public void replaceFragment(int placeHolder, Fragment fragment) {
        if (getActivity() == null)
            return;
        String tagName = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(placeHolder, fragment, tagName);
        fragmentTransaction.addToBackStack(tagName);
        fragmentTransaction.commit();
    }

    public void loadFragment(@NonNull Fragment fragment, boolean clearTillHome) {
        String fragmentName = fragment.getClass().getSimpleName();
        FragmentManager fragmentManager = getFragmentManager();
        if (isFragmentInBackStack(fragmentManager, fragment.getClass().getSimpleName())) {
            // Fragment exists, go back to that fragment
            fragmentManager.popBackStack(fragment.getClass().getSimpleName(), 0);
        } else {
            if (clearTillHome)
                fragmentManager.popBackStack(NewHomeFragment.class.getSimpleName(), 0);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment, fragmentName)
                    .addToBackStack(fragmentName)
                    .commit();
        }
    }
    public void loadMainFragment(@NonNull Fragment fragment, boolean clearTillHome) {
        String fragmentName = fragment.getClass().getSimpleName();
        String currentFragName = fragmentName;

        FragmentManager fragmentManager = getFragmentManager();
        if (isFragmentInBackStack(fragmentManager, fragment.getClass().getSimpleName())) {
            // Fragment exists, go back to that fragment
            fragmentManager.popBackStack(fragment.getClass().getSimpleName(), 0);
        } else {
            if (clearTillHome)
                fragmentManager.popBackStack(NewHomeFragment.class.getSimpleName(), 0);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame_container, fragment, fragmentName)
                    .addToBackStack(fragmentName)
                    .commit();
        }
    }

    public static boolean isFragmentInBackStack(final FragmentManager fragmentManager, final String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount(); entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void OnSuccess(Object object) {
        if (!isAlive)
            return;
        Timber.d(getString(R.string.info_api_success));
    }



    @Override
    public void onErrorHandling(Response response) {
        if (!isAlive)
            return;
        Timber.e(getString(R.string.error_api_failure));
    }

    @Override
    public void OnFailure() {
        Timber.e(getString(R.string.error_api_failure));
    }
}
