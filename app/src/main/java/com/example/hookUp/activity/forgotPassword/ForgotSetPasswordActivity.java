package com.example.hookUp.activity.forgotPassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.example.hookUp.R;
import com.example.hookUp.activity.login.LoginActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.ForgotResponse;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class ForgotSetPasswordActivity extends AppCompatActivity implements WebServiceResponse {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.et_pass)
    EditText et_pass;
    @BindView(R.id.et_re_enter_pass)
    EditText et_re_enter_pass;
    @BindView(R.id.et_otp)
    EditText et_otp;
    private String password,Repassword,otp;
    private MyDialog myDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotgot_password);
        ButterKnife.bind(this);
        myDialog=new MyDialog(this);
        settoolbar();

    }
    private void settoolbar() {
        tv_name.setText(getString(R.string.dealer_set_password));
        iv_back.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.login_button)
    public void onOtp() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            if (validation()) {
                HashMap<String,String> map=new HashMap<>();
                map.put("newPass",password);
                map.put("confirmPass",Repassword);
                map.put("forget_password_code",otp);
                new WebServiceHandler().callRememberAPI(this, HookUpPrefrences.getAuthorization(this),map);
                myDialog.ShowProgressDialog();
            }
        }
    }

    private boolean validation() {
        password=et_pass.getText().toString();
        Repassword=et_re_enter_pass.getText().toString();
        password=et_pass.getText().toString();
        otp=et_otp.getText().toString();
        if(password.isEmpty()){
            GlobalUtils.showToast(this,getString(R.string.password));
            return false;
        }else if(GlobalUtils.isValidPassword(password)){
            GlobalUtils.showToast(this,getString(R.string.pasword_valid));
            return false;
        }else if(Repassword.isEmpty()){
            GlobalUtils.showToast(this,getString(R.string.valid_email));
            return false;
        } else if (GlobalUtils.isValidPassword(Repassword)) {
            GlobalUtils.showToast(this, getString(R.string.re_pasword_valid));
            return false;
        }else if(!password.equals(Repassword)){
            GlobalUtils.showToast(this,getString(R.string.pasword_valid));
            return false;
        }else if(otp.isEmpty()){
            GlobalUtils.showToast(this,getString(R.string.valid_otp));
            return false;
        }
        return true;
    }

    @OnClick(R.id.iv_back)
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        ForgotResponse forgotResponse = (ForgotResponse) object;
        int ack = forgotResponse.getAck();
        String message = forgotResponse.getMsg();
        if (ack == 1) {
            GlobalUtils.showToastSuccess(this, message);
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void onErrorHandling(Response response) {
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(this, errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(this,"connection Failed");

    }
}
