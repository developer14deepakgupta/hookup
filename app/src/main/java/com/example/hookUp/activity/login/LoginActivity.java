package com.example.hookUp.activity.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hookUp.R;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.activity.forgotPassword.ForgotPasswordActivity;
import com.example.hookUp.activity.registration.RegistrationActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.LoginResponse.LoginEducationDetails;
import com.example.hookUp.model.bean.LoginResponse.LoginRecord;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.example.hookUp.utils.colorDialogue.AlerDilaogue;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements WebServiceResponse {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_pass)
    EditText et_pass;
    private String email, password;
    AlerDilaogue alerDilaogue;
    private MyDialog myDialog;
    @BindView(R.id.pass_check)
    CheckBox pass_check;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        myDialog = new MyDialog(this);
        settoolbar();
        intialize();

    }

    private void intialize() {
        pass_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    et_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                } else {
                    et_pass.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });
    }

    @OnClick({R.id.tv_forgot_pass, R.id.tv_click_here, R.id.btn_login})
    public void onCLick(View v) {
        if (v.getId() == R.id.tv_forgot_pass) {
            startActivity(new Intent(this, ForgotPasswordActivity.class));
        } else if (v.getId() == R.id.tv_click_here) {
            startActivity(new Intent(this, RegistrationActivity.class));
        } else if (v.getId() == R.id.btn_login) {
            hideSoftKeyboard(v);
            if (GlobalUtils.isNetworkAvailable(this)) {
                if (validation()) {
                    hitIndividualRegisration();
                }
            }

            //startActivity(new Intent(this, HomeActivity.class));
        }

    }

    private void hitIndividualRegisration() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("email", email);
            map.put("password", password);
            new WebServiceHandler().callLoginAPI(this, map);
            myDialog.ShowProgressDialog();

        }
    }

    private boolean validation() {
        email = et_email.getText().toString();
        password = et_pass.getText().toString();
        if (email.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.email));
            return false;
        } else if (password.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.password));
            return false;
        }
        return true;
    }


    private void settoolbar() {
        tv_name.setText(getString(R.string.dealer_login));
        iv_back.setVisibility(View.GONE);
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        LoginRecord loginRecord = (LoginRecord) object;
        String msg = loginRecord.getMsg();
        String message = loginRecord.getMsg();
        List<LoginEducationDetails> details = new ArrayList<>();
        int ack = loginRecord.getAck();
        if ((ack == 1)) {
            if (message.equalsIgnoreCase(getString(R.string.verify_email_address))) {
                bottomDialogBox(message);
            } else {
                details = loginRecord.getEducationDetails();
                for (int i = 0; i < details.size(); i++) {
                    HookUpPrefrences.setCollegeId(this, details.get(i).getCollegeId());
                    HookUpPrefrences.setUserUniversity(this, loginRecord.getEducationDetails().get(i).getLoginUniversityRecord().getUninversityName());
                    HookUpPrefrences.setUserCourse(this, loginRecord.getEducationDetails().get(i).getLoginCourseRecord().getCourseName());
                    HookUpPrefrences.setUserCollege(this, loginRecord.getEducationDetails().get(i).getLoginCollegeRecord().getCollegeName());
                    HookUpPrefrences.setUserStream(this, loginRecord.getEducationDetails().get(i).getLoginStreamRecord().getStreamName());
                    HookUpPrefrences.setUserYear(this, loginRecord.getEducationDetails().get(i).getLoginYearRecord().getYearName());
                }
                HookUpPrefrences.setUserId(this, loginRecord.getUserProfile().getId());
                HookUpPrefrences.setAuthorization(this, loginRecord.getSt());
                HookUpPrefrences.setUserName(this, loginRecord.getUserProfile().getFullName());
                HookUpPrefrences.setUserEmail(this, loginRecord.getUserProfile().getEmail());
                HookUpPrefrences.setUserMobile(this, loginRecord.getUserProfile().getMobile());
                if (loginRecord.getLoginBankDetails() != null) {
                    HookUpPrefrences.setAccountName(this, loginRecord.getLoginBankDetails().getAccount_name());
                    HookUpPrefrences.setAccountType(this, loginRecord.getLoginBankDetails().getAccount_type());
                    HookUpPrefrences.setUserAccountNumber(this, loginRecord.getLoginBankDetails().getAccount_no());
                    HookUpPrefrences.setIfscCode(this, loginRecord.getLoginBankDetails().getIfsc_code());
                    HookUpPrefrences.setBankName(this, loginRecord.getLoginBankDetails().getBank_name());
                    HookUpPrefrences.setUpiId(this, loginRecord.getLoginBankDetails().getUpi_id());
                }
                HookUpPrefrences.setUserLoggedIn(this, true);
                GlobalUtils.showToastSuccess(this, message);
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
        } else {
            GlobalUtils.showToastSuccess(this, message);

        }


    }

    private void bottomDialogBox(String message) {
        Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_verify_email);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView title = dialog.findViewById(R.id.title);
        Button ok_cancel = dialog.findViewById(R.id.ok_cancel);
        title.setText(message);
        ok_cancel.setOnClickListener(v -> {
            dialog.dismiss();
        });

    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(this, errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(this, "connection Failed");

    }
}
