package com.example.hookUp.activity.Home;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.hookUp.R;
import com.example.hookUp.RecycleListener;
import com.example.hookUp.activity.login.LoginActivity;
import com.example.hookUp.adapter.FirstPagerAdapter;
import com.example.hookUp.adapter.horizontalAdatper.CategoryAdapter;
import com.example.hookUp.adapter.horizontalAdatper.FirstHorizontalAdapter;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.fragment.AboutUsFragment;
import com.example.hookUp.fragment.PrivacyFragment;
import com.example.hookUp.fragment.RefundFragment;
import com.example.hookUp.fragment.SellFragment;
import com.example.hookUp.fragment.TermsFragment;
import com.example.hookUp.fragment.botton.MoreFragment;
import com.example.hookUp.fragment.buy.BuyFragment;
import com.example.hookUp.fragment.editProfile.MyProfileFragment;
import com.example.hookUp.fragment.home.NewHomeFragment;
import com.example.hookUp.fragment.myAds.MyAdsFrag;
import com.example.hookUp.fragment.product.ProductListFrag;
import com.example.hookUp.model.bean.TopCategoryProducts;
import com.example.hookUp.model.bean.TopProducts;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategoryResposne;
import com.example.hookUp.model.bean.finduniversityResponse.categoryResponse.CategotyDetail;
import com.example.hookUp.utils.CircleTransform;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.ImageLoaderUtils;
import com.example.hookUp.utils.customview.RecyclerViewMargin;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.example.hookUp.fragment.product.ProductListFrag.BUNDLE_CATEGORY_ID;
import static com.example.hookUp.fragment.product.ProductListFrag.BUNDLE_CATEGORY_NAME;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, FirstPagerAdapter.SliderClickInterface, WebServiceResponse,
        RecycleListener {
    protected NavigationView navigationView;
    protected DrawerLayout mdrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    AppBarLayout mainToolbar;
    private String currentFragName, playerImage;
    private DrawerLayout mDrawerLayout;
    @BindView(R.id.cric91_bnv_tab)
    BottomNavigationView bottomNavigationView;
    private TextView tv__nav_edit, tv_user_name;
    private FirstPagerAdapter firstPagerAdapter;
    @BindView(R.id.viewPage)
    ViewPager firstPager;
    private LinearLayout sliderDots;
    private boolean doubleBackToExitPressedOnce = false;
    private static final String TAG = HomeActivity.class.getSimpleName();
    private ImageView iv_user_image;
    @BindView(R.id.rvhome_catogry)
    RecyclerView rvhome_catogry;
    @BindView(R.id.rvhomefirst)
    RecyclerView rvhomefirst;
    int requestType = 1;
    FirstHorizontalAdapter buyDetailAdapter;
    CategoryAdapter categoryAdapter;
    List<CategotyDetail> categotyDetails = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_page);
        ButterKnife.bind(this);
        intIt();
        loadFragment(new NewHomeFragment(), false);
        setupDrawer();
        bottomnavigationdrawer();
        setUpToolBar();
        intialize();
        adapterIntialize();
    }

    private void adapterIntialize() {
        categoryAdapter = new CategoryAdapter(this, this);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 3);
        RecyclerViewMargin decoration = new RecyclerViewMargin(12, 3);
        rvhome_catogry.addItemDecoration(decoration);
        rvhome_catogry.setLayoutManager(manager);
        rvhome_catogry.setAdapter(categoryAdapter);

        LinearLayoutManager horizontalmanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvhomefirst.setLayoutManager(horizontalmanager);
        buyDetailAdapter = new FirstHorizontalAdapter(this);
        rvhomefirst.setHasFixedSize(true);
        rvhomefirst.setAdapter(buyDetailAdapter);

    }


    private void intialize() {
        tv_user_name.setText(HookUpPrefrences.getUserName(this));
        playerImage = HookUpPrefrences.getUserImage(this);
        if (playerImage != null && !playerImage.isEmpty()) {
            ImageLoaderUtils.load(playerImage, iv_user_image, new CircleTransform());
        }
        //  iv_user_image.setImageResource(Integer.parseInt(HookUpPrefrences.getUserImage(this)));
        if (GlobalUtils.isNetworkAvailable(this)) {
            new WebServiceHandler().getCategoryList(this, "0");
            requestType = 1;
        }
    }

    private void bottomnavigationdrawer() {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Fragment fragment;
                    fragment = new NewHomeFragment();
                    loadFragment(fragment, true);
//                    setTitle(getString(R.string.title_cric91));
//                    Fragment fragment = new Cric91HomeFragment();
//                    loadFragment(fragment, false);
                    break;
                case R.id.navigation_buy:
                    fragment = new BuyFragment();
                    loadMainFragment(fragment, true);
                    setTitle(getString(R.string.label_buy));
//                    setTitle(getString(R.string.title_cric91));
//                    Fragment fragment = new Cric91HomeFragment();
//                    loadFragment(fragment, false);
                    break;
                case R.id.navigation_sell:
                    fragment = new SellFragment();
                    loadMainFragment(fragment, true);
                    setTitle(getString(R.string.label_sell));
//                    setTitle(getString(R.string.title_fragment_my_matches));
//                    fragment = new Cric91MyMatchesFragment();
//                    loadFragment(fragment, true);
                    break;
                case R.id.navigation_adds:
                    fragment = new MyAdsFrag();
                    loadMainFragment(fragment, true);
                    setTitle(getString(R.string.label_adds));
                    //startActivity(new Intent(this, Cric91WalletActivity.class));
                    break;
                case R.id.navigation_more:
                    fragment = new MoreFragment();
                    loadMainFragment(fragment, true);
                    break;
            }
            return true;
        });
    }

    private void intIt() {
        navigationView = findViewById(R.id.nav_view);
        mainToolbar = findViewById(R.id.main_toolbar);
        View headerView = navigationView.getHeaderView(0);
        iv_user_image = headerView.findViewById(R.id.iv__nav_player_image);
        tv_user_name = headerView.findViewById(R.id.tv__nav_player_name);
        mdrawerLayout = findViewById(R.id.drawer_layout);
        mdrawerLayout = findViewById(R.id.drawer_layout);

    }

    private void setupDrawer() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            Bundle bundle = new Bundle();
            Fragment fragment;
            switch (menuItem.getItemId()) {
                case R.id.navigation_home:
                    fragment = new NewHomeFragment();
                    loadFragment(fragment, true);
                    mdrawerLayout.closeDrawer(GravityCompat.START);
                    setImageInToolBar(R.drawable.logo_home);
                    mainToolbar.setVisibility(View.VISIBLE);
                    break;
                case R.id.navigation_profile:
                    fragment = new MyProfileFragment();
                    loadMainFragment(fragment, true);
                    //  setImageInToolBar(R.drawable.logo_hookup);
                    break;

                case R.id.navigation_about:
                    fragment = new AboutUsFragment();
                    bundle.putString("sessionUrl", "http://13.126.248.29:1221/About_Us.pdf");
                    fragment.setArguments(bundle);
                    loadMainFragment(fragment, true);
                    break;
                case R.id.navigation_terms:
                    fragment = new TermsFragment();
                    bundle.putString("sessionUrl", "http://13.126.248.29:1221/Terms_of_Use.pdf");
                    fragment.setArguments(bundle);
                    loadMainFragment(fragment, true);
                    break;
                case R.id.navigation_privacy:
                    fragment = new PrivacyFragment();
                    bundle.putString("sessionUrl", "http://13.126.248.29:1221/Privacy_Policy.pdf");
                    fragment.setArguments(bundle);
                    loadMainFragment(fragment, true);
                    break;
                case R.id.navigation_refund:
                    fragment = new RefundFragment();
                    bundle.putString("sessionUrl", "http://13.126.248.29:1221/Refund_&_Cancellation_Policy.pdf");
                    fragment.setArguments(bundle);
                    loadMainFragment(fragment, true);
                    break;
                //   openLogoutConfirm();
                case R.id.navigation_log:
                    HookUpPrefrences.ClearPreferences(this);
                    startActivity(new Intent(this, LoginActivity.class));
                    finish();
                    //   fragment = new ContactUsFragment();

                    // loadMainFragment(fragment, true);
                    break;
            }
            mdrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        });
    }

    public void setImageInToolBar(int resId) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setLogo(resId);
        }
    }

    public void setTitleInToolBar(int resId) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbar.setTitle(resId);
            toolbar.setLogo(null);
        }
    }

    private void setUpToolBar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.logo_home);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.tool_menu));
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        mdrawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mdrawerLayout,
                toolbar, R.string.openDrawer,
                R.string.closeDrawer) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mdrawerLayout.setDrawerListener(drawerToggle);
    }

    @Override
    public void onBackPressed() {
        int backStackSize = getSupportFragmentManager().getBackStackEntryCount();
        if (mdrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mdrawerLayout.closeDrawer(GravityCompat.START);
        } else if (backStackSize == 1) {
            showBottomDialog();
        } else {
            if (backStackSize == 2) {
                setImageInToolBar(R.drawable.logo_home);
                bottomNavigationView.getMenu().getItem(0).setChecked(true);
                mainToolbar.setVisibility(View.VISIBLE);
            }
            super.onBackPressed();
        }
    }

    private void showBottomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.alertbox);
        TextView text_yes_exit = dialog.findViewById(R.id.Button_Yes);
        TextView text_No_exit = dialog.findViewById(R.id.Button_No);
        text_yes_exit.setOnClickListener(v -> {
            // gamesLog.appLogs(playerId, HomeActivity.class.getSimpleName(), GameLogConstant.APP_CLOSE, "", "", "", "");
            finishAffinity();
            dialog.dismiss();
        });
        text_No_exit.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public void loadFragment(@NonNull Fragment fragment, boolean clearTillHome) {
        String fragmentName = fragment.getClass().getSimpleName();
        currentFragName = fragmentName;
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (isFragmentInBackStack(fragmentManager, fragment.getClass().getSimpleName())) {
            // Fragment exists, go back to that fragment
            fragmentManager.popBackStack(fragment.getClass().getSimpleName(), 0);
        } else {
            if (clearTillHome)
                fragmentManager.popBackStack(NewHomeFragment.class.getSimpleName(), 0);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment, fragmentName)
                    .addToBackStack(fragmentName)
                    .commit();
        }
        mainToolbar.setVisibility(View.VISIBLE);
    }

    public void loadMainFragment(@NonNull Fragment fragment, boolean clearTillHome) {
        String fragmentName = fragment.getClass().getSimpleName();
        currentFragName = fragmentName;
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (isFragmentInBackStack(fragmentManager, fragment.getClass().getSimpleName())) {
            // Fragment exists, go back to that fragment
            fragmentManager.popBackStack(fragment.getClass().getSimpleName(), 0);
        } else {
            if (clearTillHome)
                fragmentManager.popBackStack(NewHomeFragment.class.getSimpleName(), 0);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_frame_container, fragment, fragmentName)
                    .addToBackStack(fragmentName)
                    .commit();
        }
        mainToolbar.setVisibility(View.GONE);
    }

    public static boolean isFragmentInBackStack(final FragmentManager fragmentManager, final String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount(); entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void bannerClick(int pos) {

    }

    @Override
    public void OnSuccess(Object object) {
        if (requestType == 1 && object instanceof CategoryResposne) {
            CategoryResposne categoryResposne = (CategoryResposne) object;
            categotyDetails = new ArrayList<>();
            int ack = categoryResposne.getAck();
            if (ack == 1) {
                categotyDetails = categoryResposne.getData();
                categoryAdapter.update(categotyDetails);
                getBuyList();
            }

        } else if (requestType == 2 && object instanceof TopCategoryProducts) {
            TopCategoryProducts response = (TopCategoryProducts) object;
            int ack = response.getAck();
            if (ack == 1) {
                List<TopProducts> list_product = response.getBuyProductsList();
                if (list_product != null && list_product.size() > 0) {
                    buyDetailAdapter.update(list_product);
                }

            }

        }

    }

    private void getBuyList() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            String userId = HookUpPrefrences.getUserId(this);
            String auth = HookUpPrefrences.getAuthorization(this);
            String college_id = HookUpPrefrences.getCollegeId(this);
            new WebServiceHandler().getBuyTopList(this);
            requestType = 2;


        }
    }

    @Override
    public void onErrorHandling(Response response) {
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(this, errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        GlobalUtils.showToast(this, "connection Failed");

    }


//    @OnClick(R.id.tv__nav_edit)
//    public void onEditProfile(View view ) {
//        if(view.getId()==R.id.tv__nav_edit) {
//
//        }
//    }


    @Override
    public void itemClicked(View view, int position) {
        String id = categotyDetails.get(position).getId();
        String name = categotyDetails.get(position).getCategory_name();
        goToProductList(id, name);

    }

    private void goToProductList(String id, String name) {
        Fragment fragment = new ProductListFrag();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_CATEGORY_ID, id);
        bundle.putString(BUNDLE_CATEGORY_NAME, name);
        fragment.setArguments(bundle);
        loadMainFragment(fragment, true);


    }

    @Override
    public void onClick(View v) {

    }

}
