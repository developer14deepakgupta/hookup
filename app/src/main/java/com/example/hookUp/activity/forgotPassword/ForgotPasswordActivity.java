package com.example.hookUp.activity.forgotPassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hookUp.R;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.ForgotResponse;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements WebServiceResponse {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.et_email)
    EditText et_email;
    private String email;
    private MyDialog myDialog;
    @BindView(R.id.signin_txt)
    TextView signin_txt;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);
        ButterKnife.bind(this);
        myDialog = new MyDialog(this);
        settoolbar();
    }

    private void settoolbar() {
        tv_name.setText(getString(R.string.dealer_forgot));
        iv_back.setVisibility(View.GONE);
        signin_txt.setText(getString(R.string.html));
    }

    @OnClick(R.id.login_button)
    public void onOtp() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            if (validation()) {
                myDialog.ShowProgressDialog();
                new WebServiceHandler().callForgotAPI(this, email);

            }
        }
    }

    private boolean validation() {
        email = et_email.getText().toString();
        if (email.isEmpty()) {
            GlobalUtils.showToast(this, getString(R.string.email));
            return false;
        } else if (!GlobalUtils.isValidEmail(email)) {
            GlobalUtils.showToast(this, getString(R.string.valid_email));
            return false;
        }
        return true;
    }


    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        ForgotResponse forgotResponse = (ForgotResponse) object;
        int ack = forgotResponse.getAck();
        String message = forgotResponse.getMsg();
        HookUpPrefrences.setAuthorization(this, forgotResponse.getSt());
        if (ack == 1) {
            GlobalUtils.showToastSuccess(this, message);

            startActivity(new Intent(ForgotPasswordActivity.this, ForgotSetPasswordActivity.class));
            finish();
        }
    }

    @Override
    public void onErrorHandling(Response response) {
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(this, errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(this,"connection Failed");
    }
}
