package com.example.hookUp.activity.registration;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.example.hookUp.R;
import com.example.hookUp.activity.login.LoginActivity;
import com.example.hookUp.api.WebServiceHandler;
import com.example.hookUp.api.WebServiceResponse;
import com.example.hookUp.model.bean.base.ErrorResponse;
import com.example.hookUp.model.bean.finduniversityResponse.collegeResponse.CollegeDetail;
import com.example.hookUp.model.bean.finduniversityResponse.courseResponse.CourseDetail;
import com.example.hookUp.model.bean.finduniversityResponse.courseResponse.CourseResponse;
import com.example.hookUp.model.bean.finduniversityResponse.registerResponse.RegistrationResponse;
import com.example.hookUp.model.bean.finduniversityResponse.streamResponse.StreamDetail;
import com.example.hookUp.model.bean.finduniversityResponse.collegeResponse.CollegeResponse;
import com.example.hookUp.model.bean.finduniversityResponse.streamResponse.StreamReponse;
import com.example.hookUp.model.bean.finduniversityResponse.universityResponse.UniversityDetail;
import com.example.hookUp.model.bean.finduniversityResponse.yearResponse.YearDetail;
import com.example.hookUp.model.bean.finduniversityResponse.universityResponse.UniversityResponse;
import com.example.hookUp.model.bean.finduniversityResponse.yearResponse.YearResponse;
import com.example.hookUp.utils.GlobalUtils;
import com.example.hookUp.utils.MyDialog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class RegistrationActivity extends AppCompatActivity implements WebServiceResponse {
    @BindView(R.id.login_button)
    Button login_button;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    TextView tv_year;
    @BindView(R.id.et_full_name)
    EditText et_full_name;
    @BindView(R.id.et_email_address)
    EditText et_email_address;
    @BindView(R.id.ll_spn_university)
    LinearLayout ll_spn_university;
    @BindView(R.id.ll_spn_college)
    LinearLayout ll_spn_college;
    @BindView(R.id.ll_spn_course)
    LinearLayout ll_spn_course;
    @BindView(R.id.ll_spn_stream)
    LinearLayout ll_spn_stream;
    @BindView(R.id.ll_spn_year)
    LinearLayout ll_spn_year;
    @BindView(R.id.et_mobile)
    EditText et_mobile;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.spnr_university)
    Spinner spnr_university;
    @BindView(R.id.spnr_college)
    Spinner spnr_college;
    @BindView(R.id.spnr_course)
    Spinner spnr_course;
    @BindView(R.id.spnr_stream)
    Spinner spnr_stream;
    @BindView(R.id.spnr_year)
    Spinner spnr_year;
    @BindView(R.id.checkbox_tc)
    AppCompatCheckBox checkbox_tc;

    private String fullName, email, mobile, password;
    private List<UniversityDetail> universitylist = new ArrayList<>();
    private List<CollegeDetail> collegelist = new ArrayList<>();
    private List<CourseDetail> courselist = new ArrayList<>();
    private List<StreamDetail> streamlist = new ArrayList<>();
    private List<YearDetail> yearlist = new ArrayList<>();

    ArrayList<String> university_type = new ArrayList<>();
    ArrayList<String> college_type = new ArrayList<>();
    ArrayList<String> course_type = new ArrayList<>();
    ArrayList<String> stream_type = new ArrayList<>();
    ArrayList<String> year_type = new ArrayList<>();
    int universityId = 0;
    int requestType = 0;
    private String unversityId = "0", collegeId = "0", courseId = "1", streamId = "1", yearIds = "", yearId = "1",terms;
    private MyDialog myDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regsitration);
        ButterKnife.bind(this);
        toolbar();
        intialize();
        getuniversity();
    }

    private void getuniversity() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            new WebServiceHandler().getUniversityList(this, "0");
            requestType = 1;

        }
    }

    private void intialize() {
        myDialog = new MyDialog(this);
        //  tv_year.setOnClickListener(v -> createDatePickerDialog(this, tv_year, ""));
        checkbox_tc.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (checkbox_tc.isChecked()) {
                // your code to checked checkbox
                terms = "true";
            } else {
                // your code to  no checked checkbox
                terms = "";
            }
        });


        spnr_university.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String section_selected = parent.getSelectedItem() + "";
                    String name = universitylist.get(position - 1).getUniversityName();
                    unversityId = universitylist.get(position - 1).getId();
                    if (GlobalUtils.isNetworkAvailable(RegistrationActivity.this)) {
                        new WebServiceHandler().getCollegeList(RegistrationActivity.this, unversityId);
                        requestType = 2;
                    }
                    setLinearVisibility(false, 1);
                    //Disable LinearLayout
                    ll_spn_university.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLinearVisibility(true, 1);
            }
        });
        spnr_college.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String section_selected = parent.getSelectedItem() + "";
                    String name = collegelist.get(position - 1).getCollege_name();
                    collegeId = collegelist.get(position - 1).getId();
                    if (GlobalUtils.isNetworkAvailable(RegistrationActivity.this)) {
                        new WebServiceHandler().getCourseList(RegistrationActivity.this, "0");
                        requestType = 3;
                        setLinearVisibility(false, 2);
                        //Disable LinearLayout
                        ll_spn_college.setEnabled(false);

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLinearVisibility(true, 2);
            }
        });
        // spnr course
        spnr_course.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String section_selected = parent.getSelectedItem() + "";
                    String name = courselist.get(position - 1).getCourse_name();
                    courseId = courselist.get(position - 1).getId();
                    yearIds = courselist.get(position - 1).getYear_ids();
                    if (GlobalUtils.isNetworkAvailable(RegistrationActivity.this)) {
                        new WebServiceHandler().getStreamList(RegistrationActivity.this, courseId);
                        requestType = 4;
                        setLinearVisibility(false, 3);
                        //Disable LinearLayout
                        ll_spn_course.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLinearVisibility(true, 3);
            }
        });

        //spnr stream
        spnr_stream.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String section_selected = parent.getSelectedItem() + "";
                    String name = streamlist.get(position - 1).getStream_name();
                    yearId = streamlist.get(position - 1).getId();
                    if (GlobalUtils.isNetworkAvailable(RegistrationActivity.this)) {
                        new WebServiceHandler().getYearList(RegistrationActivity.this, yearIds);
                        requestType = 5;
                        setLinearVisibility(false, 4);
                        //Disable LinearLayout
                        ll_spn_stream.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLinearVisibility(true, 4);
            }

        });

        //spnr year
        spnr_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    String section_selected = parent.getSelectedItem() + "";
                    String name = yearlist.get(position - 1).getYear_name();
                    yearId = yearlist.get(position - 1).getId();
                    setLinearVisibility(false, 5);
                    //Disable LinearLayout
                    ll_spn_stream.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLinearVisibility(true, 5);
            }
        });

    }

//    private void createDatePickerDialog(Context context, final TextView tv, String dob) {
//        final Calendar c2 = Calendar.getInstance();
//        int mYear;
//        if (dob != null) {
//            mYear = 2020;
//        } else {
//            mYear = c2.get(Calendar.YEAR);
//        }
//
//        int mMonth = c2.get(Calendar.MONTH);
//        int mDay = c2.get(Calendar.DAY_OF_MONTH);
//        DatePickerDialog datePickerDialog2 = new DatePickerDialog(context, (view, year, monthOfYear, dayOfMonth) -> {
//            String dateDynamic = formatDate(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
//            String str[] = dateDynamic.split("-");
//            String year6 = (String) str[0];
//            String month6 = (String) str[1];
//            String day6 = (String) str[2];
//            tv.setText(year6 + "-" + month6 + "-" + day6);
//        }, mYear, mMonth, mDay);
//        datePickerDialog2.show();
//
//        Calendar maxDate = Calendar.getInstance();
//        maxDate.set(Calendar.DAY_OF_MONTH, mDay);
//        maxDate.set(Calendar.MONTH, mMonth);
//        maxDate.set(Calendar.YEAR, mYear);
//        datePickerDialog2.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
//
//    }
//
//    private String formatDate(String fdate) {
//        String datetime = null;
//        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
//        SimpleDateFormat d = new SimpleDateFormat("dd-MM-yyyy");
//        try {
//            java.util.Date convertedDate = inputFormat.parse(fdate);
//            datetime = d.format(convertedDate);
//        } catch (ParseException e) {
//            Timber.e(e);
//        }
//        return datetime;
//    }


    private void toolbar() {
        tv_name.setText(getString(R.string.dealer_signup));
        iv_back.setVisibility(View.GONE);
    }

    @OnClick(R.id.login_button)
    public void onRegsiter() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            if (validation()) {
                hitIndividualRegisration();
            }
        }

    }

    private void hitIndividualRegisration() {
        if (GlobalUtils.isNetworkAvailable(this)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("full_name", fullName);
            map.put("email", email);
            map.put("password", password);
            map.put("mobile", mobile);
            map.put("university_id", unversityId);
            map.put("college_id", collegeId);
            map.put("course_id", courseId);
            map.put("stream_id", streamId);
            map.put("year_id", yearId);
            new WebServiceHandler().callRegisterAPI(this, map);
            myDialog.ShowProgressDialog();

            requestType = 6;
        }

    }

    private boolean validation() {
        fullName = et_full_name.getText().toString();
        email = et_email_address.getText().toString();
        mobile = et_mobile.getText().toString();
        password = et_password.getText().toString();
        if (fullName.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.full_name));
            return false;
        } else if (email.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.email));
            return false;
        } else if (!GlobalUtils.isValidEmail(email)) {
            GlobalUtils.showToastSuccess(this, getString(R.string.valid_email));
            return false;
        } else if (mobile.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.mobile));
            return false;
        } else if (!GlobalUtils.checkMobile(mobile)) {
            GlobalUtils.showToastSuccess(this, getString(R.string.mobile_valid));
            return false;
        } else if (password.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.password));
            return false;
        } else if (GlobalUtils.isValidPassword(password)) {
            GlobalUtils.showToastSuccess(this, getString(R.string.pasword_valid));
            return false;
        } else if (unversityId.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.university_valid));
            return false;
        } else if (collegeId.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.college_valid));
            return false;
        } else if (courseId.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.course_valid));
            return false;
        } else if (streamId.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.stream_valid));
            return false;
        } else if (yearId.isEmpty()) {
            GlobalUtils.showToastSuccess(this, getString(R.string.year_valid));
            return false;
        }else if (terms.isEmpty()) {
            GlobalUtils.showToast(getApplicationContext(), getString(R.string.checkbox_age_state));
            return false;
        }
        return true;
    }

    @Override
    public void OnSuccess(Object object) {
        myDialog.CancelProgressDialog();
        if (requestType == 1 && object instanceof UniversityResponse) {
            UniversityResponse universityResponse = (UniversityResponse) object;
            int ack = universityResponse.getAck();
            if (ack == 1) {
                universitylist = new ArrayList<>();
                universitylist = universityResponse.getData();
                university_type.clear();
                university_type.add(0, "Select University");
                for (int i = 0; i < universitylist.size(); i++) {
                    String unversityName = universitylist.get(i).getUniversityName();
                    university_type.add(unversityName);
                }
                ArrayAdapter<String> adapterUnivsersity = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1, university_type);
                adapterUnivsersity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_university.setAdapter(adapterUnivsersity);
                setLinearVisibility(false, 1);

            }

        } else if (requestType == 2 && object instanceof CollegeResponse) {
            CollegeResponse collegeResponse = (CollegeResponse) object;
            int ack = collegeResponse.getAck();
            if (ack == 1) {
                collegelist = new ArrayList<>();
                collegelist = collegeResponse.getData();
                college_type.clear();
                college_type.add(0, "Select College");
                for (int i = 0; i < collegelist.size(); i++) {
                    String unversityName = collegelist.get(i).getCollege_name();
                    college_type.add(unversityName);
                }
                ArrayAdapter<String> adapterCollege = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1, college_type);
                adapterCollege.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_college.setAdapter(adapterCollege);
                setLinearVisibility(false, 2);
            }
        } else if (requestType == 3 && object instanceof CourseResponse) {
            CourseResponse courseResponse = (CourseResponse) object;
            int ack = courseResponse.getAck();
            if (ack == 1) {
                courselist = new ArrayList<>();
                courselist = courseResponse.getData();
                course_type.clear();
                course_type.add(0, "Select Course");
                for (int i = 0; i < courselist.size(); i++) {
                    String unversityName = courselist.get(i).getCourse_name();
                    course_type.add(unversityName);
                }
                ArrayAdapter<String> adapterCourse = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1, course_type);
                adapterCourse.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_course.setAdapter(adapterCourse);
                setLinearVisibility(false, 3);
            }
        } else if (requestType == 4 && object instanceof StreamReponse) {
            StreamReponse streamReponse = (StreamReponse) object;
            int ack = streamReponse.getAck();
            if (ack == 1) {
                streamlist = new ArrayList<>();
                streamlist = streamReponse.getData();
                stream_type.clear();
                stream_type.add(0, "Select Stream");
                for (int i = 0; i < streamlist.size(); i++) {
                    String unversityName = streamlist.get(i).getStream_name();
                    stream_type.add(unversityName);
                }
                ArrayAdapter<String> adapterStream = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1, stream_type);
                adapterStream.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_stream.setAdapter(adapterStream);
                setLinearVisibility(false, 4);
            }
        } else if (requestType == 5 && object instanceof YearResponse) {
            YearResponse yearResponse = (YearResponse) object;
            int ack = yearResponse.getAck();
            if (ack == 1) {
                yearlist = new ArrayList<>();
                yearlist = yearResponse.getData();
                year_type.clear();
                year_type.add(0, "Select Year");
                for (int i = 0; i < yearlist.size(); i++) {
                    String unversityName = yearlist.get(i).getYear_name();
                    year_type.add(unversityName);
                }
                ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1, year_type);
                adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_year.setAdapter(adapterYear);
                setLinearVisibility(false, 5);
            }
        } else if (requestType == 6 && object instanceof RegistrationResponse) {
            RegistrationResponse registrationResponse = (RegistrationResponse) object;
            int ack = registrationResponse.getAck();
            String message = registrationResponse.getMsg();
            if (ack == 1) {
                GlobalUtils.showToastSuccess(this, message);
                startActivity(new Intent(this, LoginActivity.class));

            } else if (ack == 0) {
                GlobalUtils.showToast(this, message);
            }
        }

    }

    @Override
    public void onErrorHandling(Response response) {
        myDialog.CancelProgressDialog();
        try {
            ErrorResponse mError = new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
            String errorMessage = mError.getMsg();
            GlobalUtils.showToast(this, errorMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnFailure() {
        myDialog.CancelProgressDialog();
        GlobalUtils.showToast(this, "connection Failed");

    }

    public void setLinearVisibility(boolean visible, int value) {
        if (value == 1) {
            if (visible) {
                ll_spn_university.setVisibility(View.VISIBLE);
            } else {
                ll_spn_university.setVisibility(View.INVISIBLE);
            }
        } else if (value == 2) {
            if (visible) {
                ll_spn_university.setVisibility(View.VISIBLE);
                ll_spn_college.setVisibility(View.VISIBLE);
            } else {
                ll_spn_university.setVisibility(View.INVISIBLE);
                ll_spn_college.setVisibility(View.INVISIBLE);
            }
        } else if (value == 3) {
            if (visible) {
                ll_spn_university.setVisibility(View.VISIBLE);
                ll_spn_college.setVisibility(View.VISIBLE);
                ll_spn_course.setVisibility(View.VISIBLE);
            } else {
                ll_spn_university.setVisibility(View.INVISIBLE);
                ll_spn_college.setVisibility(View.INVISIBLE);
                ll_spn_course.setVisibility(View.INVISIBLE);
            }
        } else if (value == 4) {
            if (visible) {
                ll_spn_university.setVisibility(View.VISIBLE);
                ll_spn_college.setVisibility(View.VISIBLE);
                ll_spn_course.setVisibility(View.VISIBLE);
                ll_spn_stream.setVisibility(View.VISIBLE);
            } else {
                ll_spn_university.setVisibility(View.INVISIBLE);
                ll_spn_college.setVisibility(View.INVISIBLE);
                ll_spn_course.setVisibility(View.INVISIBLE);
                ll_spn_stream.setVisibility(View.INVISIBLE);
            }
        } else if (value == 5) {
            if (visible) {
                ll_spn_university.setVisibility(View.VISIBLE);
                ll_spn_college.setVisibility(View.VISIBLE);
                ll_spn_course.setVisibility(View.VISIBLE);
                ll_spn_stream.setVisibility(View.VISIBLE);
                ll_spn_year.setVisibility(View.VISIBLE);
            } else {
                ll_spn_university.setVisibility(View.INVISIBLE);
                ll_spn_college.setVisibility(View.INVISIBLE);
                ll_spn_course.setVisibility(View.INVISIBLE);
                ll_spn_stream.setVisibility(View.INVISIBLE);
                ll_spn_year.setVisibility(View.INVISIBLE);
            }
        }
    }
}
