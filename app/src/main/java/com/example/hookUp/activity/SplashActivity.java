package com.example.hookUp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.example.hookUp.activity.Home.HomeActivity;
import com.example.hookUp.R;

import com.example.hookUp.utils.HookUpPrefrences;
import com.example.hookUp.activity.login.LoginActivity;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

import static com.example.hookUp.utils.Constant.REFERRED_SCREEN;

public class SplashActivity extends AppCompatActivity {
    private String referredResponse = "";
    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Fabric.with(this, new Crashlytics());
        initView();

    }

    private void initView() {
        Thread spthread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (!HookUpPrefrences.getUserLoggedIn(SplashActivity.this)) {
                            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            intent.putExtra(REFERRED_SCREEN, referredResponse);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }
        };
        spthread.start();
    }
}
