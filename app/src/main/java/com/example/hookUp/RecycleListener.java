package com.example.hookUp;

import android.view.View;

public interface RecycleListener {
    void itemClicked(View view, int position);
}
